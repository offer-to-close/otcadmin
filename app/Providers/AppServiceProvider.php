<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function($view) {
            view()->share('view_name', $view->getName());
        });
        view()->composer('*', function($view) {
            view()->share('_viewName', $view->getName());
        });
        view()->composer('*', function($view) {
            $name = $view->getName();
            if (stripos($name, '.') !== false)
            {
                $path = explode('.', $name);
                array_pop($path);
                $path = implode('.', $path);
            }
            else $path = '';
            view()->share('_viewPath', $path);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
