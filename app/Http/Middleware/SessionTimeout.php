<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class SessionTimeout {

    protected $session;
    protected $timeout = 3600; // 1 hour timeout
    protected $adminTimeout = 1800; // 30 minute

    public function __construct(Store $session)
    {
        $this->session = $session;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $timeout = config('session.lifetime', 60) * 60;
        if (Session::has('lastActivityTime') && Auth::user() &&(time() - Session::get('lastActivityTime')) > $timeout)
        {
            auth()->logout();
            Session::clear();
        }

        Session::put('lastActivityTime', time());


        // ... Timeout Admin Portal Access ...
        $middleware = $request->route()->getAction()['middleware'] ;

        $target = 'is_admin';
        if (in_array($target, $middleware))
        {
            $adminTimeout = config('session.lifetime', 60) * 60;
            if (Session::has('lastAdminActivityTime') && Auth::user() && (time() - Session::get('lastAdminActivityTime')) > $adminTimeout)
            {
                session(['isAdmin' => false]);
            }

            Session::put('lastAdminActivityTime', time());
        }

        return $next($request);
    }
}