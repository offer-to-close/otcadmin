<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpFoundation\Response;

class apiAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $validIDs = ['otc-2', 'oh-7'];
        $id = $request->route('apiUserID');
        $result = in_array($id, $validIDs);
        if (!$result) return new Response('Invalid Access Attempted: ' .$id);
        return $next($request);
    }
}