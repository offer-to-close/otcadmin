<?php

namespace App\Http\Controllers\OTC;

use App\Combine\AccountCombine;
use App\Combine\TransactionCombine;
use App\Http\Controllers\CredentialController;
use App\Http\Controllers\InvitationController;
use App\Http\Controllers\OTC\MailController;
use App\Library\common\Utilities\_LaravelTools;
use App\Models\LoginInvitation;
use App\Models\OTC\Agent;
use App\Models\OTC\Buyer;
use App\Models\OTC\Escrow;
use App\Models\OTC\Loan;
use App\Models\OTC\lu_UserRoles;
use App\Models\OTC\MemberRequest;
use App\Models\OTC\Property;
use App\Models\OTC\Seller;
use App\Models\OTC\Title;
use App\Models\OTC\Transaction;
use App\Models\OTC\TransactionCoordinator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use App\Library\common\Utilities\DisplayTable;

class MenuController extends Controller
{

    public function reviewMemberRequests(Request $request)
    {
        $view = 'otc.memberRequest.reviewMemberRequest';

        $requests =  self::getMemberRequests()->toArray();

        $excludeCol = ['TransactionSide', 'isApproved', 'isRejected', 'DecisionReason', 'DeciderUsers_ID', 'DateDecision',
                       'Decision_LogMail_ID', 'isTest', 'DateUpdated', 'deleted_at'];

        foreach($requests as $idx=>$row)
        {
            $r = [];
            if($row['NameFirst'] == $row['NameLast'])
            {
                $this->markRequestSpam($row['ID']);
                unset($requests[$idx]);
                continue;
            }
            foreach($row as $key=>$val)
            {
                if (in_array($key, $excludeCol)) continue;
                $r[$key] = $val;
            }
            $r['action']  = '<a href="'.route('otc.approveRequest', ['memReqID'=>$row['ID']]).'"><i class="far fa-thumbs-up"></i></a>';
            $r['action'] .= '&nbsp;&nbsp;' . '<a href="'.route('otc.rejectRequest', ['memReqID'=>$row['ID']]).'"><i class="far fa-thumbs-down"></i></a>';
//            $r['action'] .= '&nbsp;' . '<i style="cursor:pointer;" class="fas fa-ban mark_spam" data-id="'.$r['ID'].'"></i>';
            $r['action'] .= '&nbsp;&nbsp;' . '<a href="'.route('otc.markRequestSpam', ['memReqID'=>$row['ID']]).'"><i class="fas fa-ban mark_spam"></i></a>';

            $requests[$idx] = $r;
        }
//        dump($r);
        $tableTransaction = DisplayTable::getBasic($requests, ['excludeColumns'=>$excludeCol]);

        $view = _LaravelTools::addVersionToViewName($view);

//        dd(['view'=>$view,__METHOD__=>__LINE__]);
        return view($view,
            ['statusTable' => $tableTransaction,
             'totalCount'=>count($requests),
            ]);

    }

    public function approveRequest(Request $request, $memReqID=0, $apiCall=false)
    {
        $view = 'otc.memberRequest.requestedInvitation';

        Log::debug(['memReqID'=>$memReqID,__METHOD__=>__LINE__]);
        if (!is_null($memReqID) && is_numeric($memReqID) && $memReqID > 0)
        {

            Log::debug([__METHOD__=>__LINE__]);
            $memReq = MemberRequest::find($memReqID);

            if (!empty($memReq) && count($memReq->toArray()) > 0)
            {
                $transaction = false;

                $rv = $this->memberRequestApiCall($memReqID, 'approve');

                return redirect(route('otc.reviewMemberRequest'));
            }
        }
    }
    public function memberRequestApiCall($memberRequestID, $action)
    {
        $action = strtolower($action);
        if (!in_array($action, ['approve', 'reject'])) $action = 'reject';

        $url = config('otcServices.dbServices.otc')['apiUrl'];

        $url = str_replace('{}', env('AdminHandshake'), $url) . 'member-request/'.$memberRequestID.'/'.$action;

        Log::debug(['url'=>$url , __METHOD__=>__LINE__]);
        $headers = ["Content-Type: application/json"];

        $fh = fopen(storage_path('___curl___.log'), 'w');

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_URL, $url );
//        curl_setopt($curl, CURLOPT_VERBOSE, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
//        curl_setopt($curl, CURLOPT_STDERR, $fh);
        if (!isServerLocal())
        {
            $pw = config('otcCommon.constants.API_PW.otc');
            curl_setopt($curl, CURLOPT_USERPWD, $pw);
        }

        $rv = curl_exec($curl);

        Log::debug(['curl'=>$rv,__METHOD__=>__LINE__]);
 //       dd(['curl'=>$rv,__METHOD__=>__LINE__]);
        return $rv;

    }

    public function rejectRequest(Request $request, $memReqID=0)
    {
        $route = 'otc.reviewMemberRequest';
        Log::debug(['id' => $memReqID, __METHOD__ => __LINE__]);

        if ($memReqID == 0) return redirect()->back();
        $rv = $this->memberRequestApiCall($memReqID, 'reject');

        return redirect(route($route));
    }
    public static function getMemberRequests($justNew=true)
    {
        $memRequest = new MemberRequest();
//        try {Log::debug([$memRequest->getConnection()->getConfig(), __METHOD__=>__LINE__]);}  catch (\Exception $e) {log::debug('Error logging connection; ' .
//                                                                                                                   __METHOD__);}
        $query =  $memRequest->orderby('ID');

        if ($justNew) $query->where('isApproved', 0)->where('isRejected', 0);
        else
        {
            $query->where( function($q) use ($query) {
                $q->where('isApproved', 1)
                  ->orwhere('isRejected', 1);
            });
        }
//            Log::debug(['sql'=>$query->toSql(),__METHOD__=>__LINE__]);
        return $query->get();
    }
    public function markRequestSpam($ID)
    {
        if(!$ID) return response()->json([
            'status'    => 'fail',
            'message'   => 'Record does not exist, no ID. Error code:  mrk-s-001',
        ]);
        $deleted = MemberRequest::where('ID', $ID)->update([
            'deleted_at'        => date('Y-m-d h:i:s'),
            'isRejected'        => 1,
            'DecisionReason'    => 'spam',
            'DeciderUsers_ID'   => auth()->id() ?? 0,
            'DateDecision'      => date('Y-m-d h:i:s'),
        ]);
        if($deleted)
        {
            $route = 'otc.reviewMemberRequest';
            return redirect(route($route));
        }
        else
        {
            return response()->json([
                'status'    => 'fail',
                'message'   => 'Unable to mark as spam. Error code: mrk-s-002',
            ]);
        }
    }
    public function sendInvitationFromRequest(Request $request)
    {
        $request->validate([
            'memberRequestID'  => 'required|integer|min:1',
        ]);
        $data          = $request->all();

        if ($data['inviteButton'] != 'cancelInvitation')
        {
            if (!is_null($data['memberRequestID']) && is_numeric($data['memberRequestID']) && $data['memberRequestID'] > 0)
            {
                $memReq                  = MemberRequest::find($data['memberRequestID']);
                $memReq->isApproved      = true;
                $memReq->isRejected      = false;
//                $memReq->DeciderUsers_ID = CredentialController::current()->ID();
                $memReq->upsert($memReq->toArray());
                $mr = MemberRequest::find($data['memberRequestID']);

                $inviteCode = $this->makeInviteCodeFromRequest($data['memberRequestID']);
                MailController::sendRequestedInvitation($data['memberRequestID'], $inviteCode, true, false);
            }
        }
        return redirect(route('staff.menuProcess', ['path'=>'reviewMembershipRequest']));
    }
    public function makeInviteCodeFromRequest($memberRequestID)
    {
        if (!is_numeric($memberRequestID))
        {
            //ToDo
        }
        $roleID = $userID = 0;
        $userType = 'u'; //the default user type (user)
        $memReq = MemberRequest::where('ID', '=', $memberRequestID)->get()->first();
        if (empty($memReq)) ddd(['&& Error &&','memberRequestID'=>$memberRequestID, 'memReq'=>$memReq]);
        $transactionID = ($memReq->TransactionID > 0) ? $memReq->TransactionID : 0;
        $role = (strlen($memReq->RequestedRole) > 0) ? $memReq->RequestedRole : '';

        $array = [
            'transactionID'     => $transactionID,
            'requestID'         => $memberRequestID,
            'inviteeRole'       => $role,
            'inviteeRoleID'     => $roleID,
            'inviteeUserID'     => $userID,
            'shareRoomID'       => $transactionID,
            'userType'          => $userType,
            'checksum'          => date('is'),
        ];

        $json = str_rot13(json_encode($array));
        $invitation_string = rawurlencode($json);
        $this->recordInvitation($invitation_string,$transactionID);
        //dd($invitation_string);
        return $invitation_string;
    }

    public function recordInvitation($inviteCode, $transactionID = 0)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in

        $inviteCodeArray = $this->translateInviteCode($inviteCode);
        if ($transactionID > 0)
        {
            $invitee = TransactionCombine::getPerson($inviteCodeArray['inviteeRole'], $transactionID);
            $invitee = $invitee['data']->first();
            $inviteeUserID = empty($invitee->Users_ID) ? 0 : $invitee->Users_ID;
        }

        //dd(['inviteCode'=>$inviteCode, 'taid'=>$transactionID,'inviteCodeArray'=>$inviteCodeArray,'invitee'=>$invitee, 'inviteeUserID'=>$inviteeUserID]);

        $currentRole = TransactionCombine::getTransactionRole($transactionID);
        $inviter = TransactionCombine::getPerson($currentRole, $transactionID);
        $inviter = $inviter['data']->first();
        if (isset($inviteCodeArray['requestID'])) $memReq = \App\Models\MemberRequest::find($inviteCodeArray['requestID']);
        else $memReq = NULL;
        if(!is_null($memReq) && $transactionID === 0)
        {
            $nameFirstInvitee = $memReq->NameFirst  ?? '';
            $nameLastInvitee  = $memReq->NameLast   ?? '';
            $emailInvitee     = $memReq->Email      ?? '';
        }

        $model                          = [];
        $model['InviteCode']            = $inviteCode;
        $model['InvitedBy_ID']          = CredentialController::current()->ID();
        $model['InvitedByRole']         = session('userRole');
        $model['InviteeRole']           = $inviteCodeArray['inviteeRole'];
        $model['InviteeRole_ID']        = $inviteCodeArray['inviteeRoleID'];
        $model['NameFirstInvitee']      = $invitee->NameFirst ?? $nameFirstInvitee ?? '';
        $model['NameLastInvitee']       = $invitee->NameLast ?? $nameLastInvitee ?? '';
        $model['EmailInvitee']          = $invitee->Email ?? $emailInvitee ?? '';
        $model['EmailInviter']          = $inviter->Email ?? '';
        $model['Transactions_ID']       = $inviteCodeArray['transactionID'] ?? '';
        $model['DateInvite']            = date('Y-m-d H:i:s');
        $model['DateInvitationExpires'] = date('Y-m-d H:i:s', strtotime('+1 week'));
        $model['DateCreated']           = date('Y-m-d H:i:s');

        $loginInvitation = new LoginInvitation();
        try
        {
            $lI = $loginInvitation->upsert($model);
            $a = LoginInvitation::where('InviteCode', $inviteCode)->get();
            //Log::debug(['inviteCode'=>$inviteCode, 'upsert return '=>$lI->toArray(), 'LoginInvitations'=>$a->toArray()]);
        }
        catch (\Exception $e)
        {
            //currently empty to avoid failure but needs thoughtful error handling
            ddd(['Exception'=>$e->getMessage()]);
        }
    }

}
