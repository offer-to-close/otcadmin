<?php

namespace App\Http\Controllers\OTC;

use App\Combine\DocumentCombine;
use App\Combine\ShareRoomCombine;
use App\Http\Controllers\AccessController;
use App\Http\Controllers\CredentialController;
use App\Http\Controllers\DocumentAccessController;
use App\Http\Controllers\InvitationController;
use App\Library\otc\TableTools;
use App\Library\common\Utilities\_Convert;
use App\Library\common\Utilities\_LaravelTools;
use App\Models\lk_Transactions_Documents;
use App\Models\Model_Parent;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class DevController extends Controller
{
    private $schemaHackFile = 'OTC-DB-20181113.sql';
    private static $accessLevel = 'd';  // Dev Access
    private static $viewPath = 'developer';
    private static $prefix = 'dev';

    public function processToolMenu(Request $request)
    {
        //       if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        if (!AccessController::hasAccess(self::$accessLevel)) // requires Dev access
        {
            Session::flash('error','You have no access to this transaction. Please try again.');
            Session::push('flash.old','error');
            return redirect()->back();
        }

        $data = $request->all();
        if (isset($data['btnAction']))
        {
            $method = str_replace(self::$prefix.'.', null, $data['btnAction']);

            return $this->{$method}();
        }
    }

    public function viewMenu(Request $request)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        if (!AccessController::hasAccess(self::$accessLevel)) // requires Dev access
        {
            Session::flash('error', 'You have no access to this transaction. Please try again.');
            Session::push('flash.old','error');
            return redirect()->back();
        }

        $view = 'dashboard.'.self::$viewPath.'.menu';
        return (view(_LaravelTools::addVersionToViewName($view)));
    }

    public function shareRoomDocs()
    {
        if (!AccessController::hasAccess(self::$accessLevel)) // requires Dev access
        {
            Session::flash('error','You have no access to this transaction. Please try again.');
            Session::push('flash.old','error');
            return redirect()->back();
        }
        $request       = request();
        $expectedInput = ['transactionID',];
        $view = 'dashboard.'.self::$viewPath.'.tools.' . __FUNCTION__;

        $data = $request->all();

        $docs = [];
        if (isset($data['transactionID']))
        {
            $formData = [];
            foreach ($expectedInput as $field)
            {
                if (!isset($data[$field])) continue;
                $formData[$field] = $data[$field];
            }

            $docs = ShareRoomCombine::getVisibleDocuments($data['transactionID'], CredentialController::current()->ID());
        }
        else $formData = null;
        // ddd($docs);
        return view(_LaravelTools::addVersionToViewName($view),
            ['formData'      => $formData,
             'data'          => _Convert::toArray($docs),
             'sourceTitle'   => 'Table = bag_TransactionDocument',
             'transactionID' => $data['transactionID'] ?? null,
             'route'         => request()->route()->getName(),
             'btnAction'     => 'dev.' . __FUNCTION__,
            ]);
    }

    public function transactionRoles()
    {
        $view = 'dashboard.'.self::$viewPath.'.tools.' . __FUNCTION__;

        return view(_LaravelTools::addVersionToViewName($view));
    }

    public function dataModel()
    {
        $view = 'dashboard.'.self::$viewPath.'.tools.' . __FUNCTION__;
        $dataModel = Model_Parent::dataModel();

        $data = [];
        foreach ($dataModel as $tbl=>$fields)
        {
            foreach($fields as $field=>$target)
            {
                $data[] = ['Table/Column'=>$tbl . '.' . $field, 'Reference'=>$target];
            }
        }
        return view(_LaravelTools::addVersionToViewName($view), [
            'data'=>$data,
            'sourceTitle'=>'Data Model',
        ]);
    }

    public function testApi()
    {
        if (!AccessController::hasAccess(self::$accessLevel)) // requires Dev access
        {
            Session::flash('error', 'You have no access to this transaction. Please try again.');
            Session::push('flash.old','error');
            return redirect()->back();
        }
        $request       = request();
        $expectedInput = ['url', 'payload'];
        $view          = 'dashboard.developer.tools.' . __FUNCTION__;
        $defUrl = 'http://test.offertoclose.com/api/agent-insert';
        $data = ['NameFirst'=>'Marc', 'NameLast'=>'Gelman', 'License'=>'OTC-000099', 'isTest'=>true];
        $accessID     = 'xchop-temporary-access';
        $defPayload   = $accessID.'/'.json_encode($data);


        $data = $request->all();

        $docs = [];
        if (isset($data['url']))
        {
            $formData = [];
            foreach ($expectedInput as $field)
            {
                if (!isset($data[$field])) continue;
                $formData[$field] = $data[$field];
            }
            $docs = lk_Transactions_Documents::documentsByTransactionId($data['transactionID']);
        }
        else $formData = null;
        // ddd($docs);
        return view(_LaravelTools::addVersionToViewName($view),
            ['formData'      => $formData,
             'return '          => '',
             'sourceTitle'   => 'insert-agent API',
             'url' => $defUrl ?? null,
             'payload' => $defPayload ?? null,
             'route'         => request()->route()->getName(),
             'btnAction'     => 'dev.' . __FUNCTION__,
            ]);
    }

    public function transactionDocs()
    {
        if (!AccessController::hasAccess(self::$accessLevel)) // requires Dev access
        {
            Session::flash('error', 'You have no access to this transaction. Please try again.');
            Session::push('flash.old','error');
            return redirect()->back();
        }
        $request       = request();
        $expectedInput = ['transactionID',];
        $view          = 'dashboard.developer.tools.' . __FUNCTION__;

        $data = $request->all();

        $docs = [];
        if (isset($data['transactionID']))
        {
            $formData = [];
            foreach ($expectedInput as $field)
            {
                if (!isset($data[$field])) continue;
                $formData[$field] = $data[$field];
            }
            $docs = lk_Transactions_Documents::documentsByTransactionId($data['transactionID']);
        }
        else $formData = null;
        // ddd($docs);
        return view(_LaravelTools::addVersionToViewName($view),
            ['formData'      => $formData,
             'data'          => _Convert::toArray($docs),
             'sourceTitle'   => 'Table = lk_Transactions-Documents',
             'transactionID' => $data['transactionID'] ?? null,
             'route'         => request()->route()->getName(),
             'btnAction'     => 'dev.' . __FUNCTION__,
            ]);
    }

    public function shareRooms()
    {
        if (!AccessController::hasAccess(self::$accessLevel)) // requires Dev access
        {
            Session::flash('error', 'You have no access to this transaction. Please try again.');
            Session::push('flash.old','error');
            return redirect()->back();
        }
        $request       = request();
        $expectedInput = ['',];
        $view = 'dashboard.'.self::$viewPath.'.tools.' . __FUNCTION__;

        $data = $request->all();

        $rooms = [];
        $formData = [];

        $rooms = DB::table('ShareRooms')
                   ->leftJoin('Transactions', 'ShareRooms.Transactions_ID', '=', 'Transactions.ID')
                   ->leftJoin('Properties', 'Transactions.Properties_ID', '=', 'Properties.ID')
                   ->leftJoin('bag_TransactionDocuments', 'ShareRooms.bag_TransactionDocuments_ID', '=', 'bag_TransactionDocuments.ID')
                   ->leftJoin('lk_Transactions-Documents', 'bag_TransactionDocuments.lk_Transactions-Documents_ID', '=', 'lk_Transactions-Documents.ID')
                   ->leftJoin('Documents', 'lk_Transactions-Documents.Documents_Code', '=', 'Documents.Code')
                   ->select('Transactions.ID as Transaction #', 'Properties.Street1', 'Properties.City',
                       'Documents.ShortName', 'Documents.Description',
                       'bag_TransactionDocuments.SignedBy','bag_TransactionDocuments.Notes',
                       'ShareRooms.isRoomOpen', 'ShareRooms.isAvailable')
                   ->where('ShareRooms.isTest', false)
                   ->where('ShareRooms.isAvailable', true)
                   ->orderBy('ShareRooms.Transactions_ID', 'desc')
                   ->orderBy('bag_TransactionDocuments_ID')
                   ->get();

        // ddd($docs);
        return view(_LaravelTools::addVersionToViewName($view),
            ['formData'      => $formData,
             'data'          => _Convert::toArray($rooms),
             'sourceTitle'   => 'Table = ShareRooms',
             'route'         => request()->route()->getName(),
             'btnAction'     => self::$prefix . '.' . __FUNCTION__,
            ]);
    }

    public function transactionFiles()
    {
        if (!AccessController::hasAccess(self::$accessLevel)) // requires Dev access
        {
            Session::flash('error', 'You have no access to this transaction. Please try again.');
            Session::push('flash.old','error');
            return redirect()->back();
        }

        $request       = request();
        $expectedInput = ['transactionID',];
        $view = 'dashboard.'.self::$viewPath.'.tools.' . __FUNCTION__;

        $data = $request->all();

        $info = [];
        if (isset($data[reset($expectedInput)]))
        {
            $formData = [];
            $query = DB::table('bag_TransactionDocuments')
                       ->join('lk_Transactions-Documents', 'bag_TransactionDocuments.lk_Transactions-Documents_ID', '=', 'lk_Transactions-Documents.ID')
                       ->join('Documents', 'lk_Transactions-Documents.Documents_Code', '=', 'Documents.Code')
                       ->select('bag_TransactionDocuments.*',
                           'lk_Transactions-Documents.Documents_Code',
                           'Documents.Description')
                       ->where('Transactions_ID', '=', $data['transactionID'] ?? 0)
                       ->orderBy('UploadedByRole')
                       ->orderBy('bag_TransactionDocuments.DateCreated', 'desc');
            $info = $query->get();
            //            ddd(['sql'=>$query->toSql(), 'info'=>$info]);
        }
        else $formData = null;

        return view(_LaravelTools::addVersionToViewName($view),
            ['formData'      => $formData,
             'data'          => _Convert::toArray($info),
             'transactionID' => $data['transactionID'] ?? '?',
             'sourceTitle'   => 'Table = bag_TransactionDocuments',
             'route'         => request()->route()->getName(),
             'btnAction'     => self::$prefix . '.' . __FUNCTION__,
            ]);
    }

    public function users()
    {
        $view = 'dashboard.'.self::$viewPath.'.tools.' . __FUNCTION__;
        $data = User::all();
        $excludeColumns = ['created_at','updated_at', 'LoginInvitations_ID',  'image', 'NameFirst', 'NameLast', 'isTest'];
        $table = TableTools::editTable($data->toArray(),
            ['excludeColumns'=>$excludeColumns,
             'primaryKey'=>'id',
             'showEditButton'=>false,
             'showZero'=>true, ]);
        return view(_LaravelTools::addVersionToViewName($view), ['table'=>$table]);
    }

    public function routes()
    {
        $view = 'dashboard.'.self::$viewPath.'.tools.' . __FUNCTION__;
        $view = _LaravelTools::addVersionToViewName($view);

        foreach(_LaravelTools::routesAsArray() as $name=>$uri)
        {
            $routes[] = ['Route Name'=>$name, 'URI'=>$uri];
        }
        return view($view,
            [
                'sourceTitle' => 'Route List',
                'routes'    => $routes,
            ]);
    }

    public function userTransactions()
    {
        $view = 'dashboard.'.self::$viewPath.'.tools.' . __FUNCTION__;
        return view(_LaravelTools::addVersionToViewName($view));
    }

    public function agentTest()
    {
        $view = 'dashboard.'.self::$viewPath.'.tools.' . __FUNCTION__;
        $view = _LaravelTools::addVersionToViewName($view);

        $lines = file_get_contents('http://localhost/OfferToClose/public/api/agent/mzev/1');
        ddd($lines);
        return view(_LaravelTools::addVersionToViewName($view));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function schemaHack()
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        if (!AccessController::hasAccess(self::$accessLevel)) // requires Dev access
        {
            Session::flash('error', 'You have no access to this transaction. Please try again.');
            Session::push('flash.old','error');
            return redirect()->back();
        }

        $view         = 'devTools.hackSchema';
        $inputFile    = public_path('_imports/schema/') . $this->schemaHackFile;
        $outputFile   = 'display';
        $outputSchema = $this->addForeignKeysToSchema($inputFile, $outputFile);

        if ($outputFile == 'display')
        {
            return view($view,
                [
                    'inputFile'    => $inputFile,
                    'outputFile'   => $outputFile,
                    'pageMode'     => 'display',
                    'outputSchema' => is_array($outputSchema) ? implode('<br/>', $outputSchema) : $outputSchema,
                ]);
        }
    }

    /**
     * Take real DB schema and create the foreign keys necessary based on the field naming conventions and some special rules so that a
     * generic SQL documentation tool can render an accurate model
     *
     * @param      $inputFile
     * @param null $outputFile If equals "display" it displays the new schema on the screen
     *
     * @return array|bool
     */
    private function addForeignKeysToSchema($inputFile, $outputFile = null, $fakeTableName = 'otc-demo')
    {
        if (!AccessController::hasAccess(self::$accessLevel)) // requires Dev access
        {
            Session::flash('error', 'You have no access to this transaction. Please try again.');
            Session::push('flash.old','error');
            return redirect()->back();
        }

        $ignore = ['UploadedBy'];

        if (!is_file($inputFile))
        {
            return false;
        }

        $schema = file($inputFile);
        $fields = $tableList = [];
        $table  = null;
        foreach ($schema as $idx => $line)
        {
            $line = trim($line);
            if ($table)
            {
                if (substr($line, 0, 1) == ')')
                {
                    $table = null;
                    continue;
                }
                if (substr($line, 0, 1) == '`')
                {
                    $fieldName = substr($line, 1);
                    list($fieldName) = explode('`', $fieldName, 2);
                    if (strpos($fieldName, '_') !== false && substr($fieldName, -3) != '_at'
                        && substr($fieldName, -6) != '_token')
                    {
                        $fields[$table][] = $fieldName;
                    }
                }
            }
            else
            {
                if (stripos($line, 'CREATE TABLE') !== false)
                {
                    list($junk, $table, $junk) = explode('`', $line, 3);
                    $tableList[] = $table;
                    continue;
                }
                if (stripos($line, 'CREATE DATABASE') !== false)
                {
                    list($junk, $tableName, $junk) = explode('`', $line, 3);
                    continue;
                }
            }
        }

        foreach ($fields as $table => $flds)
        {
            foreach ($flds as $fld)
            {
                $cnt             = substr_count($fld, '_');
                $c[$table][$fld] = $cnt;
                if ($cnt > 1)
                {
                    list($f1, $f2, $key) = explode('_', $fld);
                    $f = implode('_', [$f1, $f2]);
                }
                else list($f, $key) = explode('_', $fld);

                if ($f == 'Users') $f = 'users';
                if (($tmp = stripos($f, 'TransactionCoordinators')) !== false && $tmp != 0) $f = 'TransactionCoordinators';
                if ($fld == 'TaskSets_Value') $f = 'lu_TaskSets';
                if ($fld == 'ApprovedByUsers_ID') $f = 'users'; //changed the field to ApprovedByUsers_ID todo

                if (in_array($f, $ignore)) continue;
                $schema[] = null;
                $schema[] = 'ALTER TABLE `' . $table . '`';
                $schema[] = 'ADD FOREIGN KEY `fk_' . $fld . '`(`' . $fld . '`)';
                $schema[] = 'REFERENCES `' . $f . '`(' . $key . ');';

            }
        }
        //       ddd($c);

        if (!empty($fakeTableName))
        {
            $schema = str_replace($tableName, $fakeTableName, $schema);
        }
        //       ddd([$tableName, $fakeTableName, reset($schema)]);
        return $schema;
    }
    public function bagDocs($bagID=null)
    {
        if (!AccessController::hasAccess(self::$accessLevel)) // requires Dev access
        {
            Session::flash('error', 'You have no access to this transaction. Please try again.');
            Session::push('flash.old','error');
            return redirect()->back();
        }

        $request       = request();
        $expectedInput = ['bagID',];
        $view = 'dashboard.'.self::$viewPath.'.tools.' . __FUNCTION__;

        $data = $request->all();

        $docs = [];
        //dd($data);
        if (isset($data[reset($expectedInput)]))
        {
            $formData = [];
            foreach ($expectedInput as $field)
            {
                if (!isset($data[$field])) continue;
                $formData[$field] = $data[$field];
            }
            $docs = DocumentCombine::getBagDocument($data['bagID'])->toArray();
            $docs = _Convert::toArray($docs);

            foreach ($docs as $idx=>$row)
            {
                $docs[$idx]['AccessSum'] =
                    implode(', ',
                        array_column(DocumentAccessController::decodeAccessSum($row['AccessSum']), 'display'));
                $docs[$idx]['TransferSum'] =
                    implode(', ',
                        array_column(DocumentAccessController::decodeTransferSum($row['TransferSum']), 'value'));
            }
        }
        else $formData = null;

        return view(_LaravelTools::addVersionToViewName($view),
            ['formData'    => $formData,
             'data'        => _Convert::toArray($docs),
             'sourceTitle' => 'Table = bag_TransactionDocument',
             'bagID'       => $data['bagID'] ?? null,
             'route'       => request()->route()->getName(),
             'btnAction'   => 'dev.' . __FUNCTION__,
            ]);
    }
    public function makeInviteCode()
    {
        if (!AccessController::hasAccess(self::$accessLevel)) // requires Dev access
        {
            Session::flash('error', 'You have no access to this transaction. Please try again.');
            Session::push('flash.old','error');
            return redirect()->back();
        }

        $request       = request();
        $expectedInput = ['transactionID', 'role', 'roleID', 'userID', 'shareRoomID', 'userType'];
        $view = 'dashboard.'.self::$viewPath.'.tools.' . __FUNCTION__;

        $data = $request->all();

        $info = [];
        if (isset($data[reset($expectedInput)]))
        {
            $iCode = (new InvitationController())->makeInviteCode($data['transactionID'],
                $data['role'],
                $data['roleID'],
                $data['userID'],
                $data['shareRoomID'],
                $data['userType']);
        }
        else $formData = $iCode = null;

        return view(_LaravelTools::addVersionToViewName($view),
            ['formData' => $data,
             'inviteCode'    => $iCode,
             'data'          => [$data['transactionID']??null,
                                 $data['role']??null,
                                 $data['roleID']??null,
                                 $data['userID']??null,
                                 $data['shareRoomID']??null,
                                 $data['userType']??null
             ],
             'sourceTitle'   => 'Create an Invite Code',
             'route'         => request()->route()->getName(),
             'btnAction'     => self::$prefix . '.' . __FUNCTION__,
            ]);
    }

}
