<?php

namespace App\Http\Controllers\OTC;

use App\Combine\MilestoneCombine;
use App\Combine\ShareRoomCombine;
use App\Combine\TaskCombine;
use App\Http\Controllers\AccessController;
use App\Http\Controllers\CredentialController;
use App\Http\Controllers\InvitationController;
use App\Http\Controllers\UserController;
use App\Library\common\Utilities\_Convert;
use App\Library\common\Utilities\_LaravelTools;
use App\Library\common\Utilities\DisplayTable;
use App\Models\Task;
use App\Models\Transaction;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    private static $accessLevel = 'a';  // Admin Access
    private static $viewPath = 'admin';
    private static $prefix = 'admin';
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function admin()
    {
        $view = 'admin.dashboard';
        return view(_LaravelTools::addVersionToViewName($view));
    }
    public function adminLogout()
    {
        session(['isAdmin'=>false]);
        return redirect(route(config('otc.DefaultRoute.dashboard')));
    }

    public function editOtcUsers()
    {}
    public function viewTransactions()
    {

    }

    /**
     * @param int  $transactionID
     * @param null $role
     * @param int  $roleID
     * @param int  $userID
     * @param int  $shareRoomID
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getInviteCode($transactionID = 0, $role = null, $roleID = 0, $userID = 0, $shareRoomID = 0)
    {
        if (env('APP_ENV') != 'local' && !\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in

        $inviteController = new InvitationController();
        $inviteCode       = $inviteController->makeInviteCode($transactionID,
            $role, $roleID, $userID, $shareRoomID);
        return view('tools/adminDump', ['dump' => var_export(['inviteCode' => $inviteCode])]);
    }

    public function enterAdmin()
    {
        $view = 'dashboard.adminDashboard';
        return _LaravelTools::addVersionToViewName($view);
    }

    public function processMenu(Request $request)
    {
        $data = $request->all();
        ddd($data);
    }

    public function processToolMenu(Request $request)
    {
        //       if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        if (!AccessController::hasAccess(self::$accessLevel)) // requires Admin access
        {
            Session::flash('error', 'You have no access to this transaction. Please try again.');
            Session::push('flash.old','error');
            return redirect()->back();
        }

        $data = $request->all();
        if (isset($data['btnAction']))
        {
            $method = str_replace(self::$prefix.'.', null, $data['btnAction']);

            return $this->{$method}();
        }
    }

    public function viewMenu(Request $request)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        if (!AccessController::hasAccess(self::$accessLevel)) // requires Admin access
        {
            Session::flash('error', 'You have no access to this transaction. Please try again.');
            Session::push('flash.old','error');
            return redirect()->back();
        }

        $view = 'dashboard.admin.menu';
        return (view(_LaravelTools::addVersionToViewName($view)));
    }

    public function documentDefaults()
    {
        $view = 'dashboard.'.self::$viewPath.'.tools.' . __FUNCTION__;
        return view(_LaravelTools::addVersionToViewName($view));
    }

    public function summaries()
    {
        $view = 'dashboard.'.self::$viewPath.'.tools.' . __FUNCTION__;

        $transactionStatus      = Transaction::count([], true);
        $tableTransactionStatus = DisplayTable::getBasic($transactionStatus, ['showNull'=>true,]);

        return view(_LaravelTools::addVersionToViewName($view),
            ['statusTable' => $tableTransactionStatus]);
    }
    public function taskDefaults()
    {
        if (!AccessController::hasAccess(self::$accessLevel))
        {
            Session::flash('error', 'You have no access to this transaction. Please try again.');
            Session::push('flash.old','error');
            return redirect()->back();
        }
        $request       = request();
        $expectedInput = ['state', 'role'];
        $view = 'dashboard.'.self::$viewPath.'.tools.' . __FUNCTION__;

        $data = $request->all();

        $docs = [];
        $tasks = [];
        if (isset($data[reset($expectedInput)]))
        {
            $formData = [];
            foreach ($expectedInput as $field)
            {
                if (!isset($data[$field])) continue;
                $formData[$field] = $data[$field];
            }
            $tasks = Task::where('State', $formData['state'])->where('TaskSets_Value', $formData['role'])->get();

        }
        else $formData = null;
        //dd($tasks);
        return view(_LaravelTools::addVersionToViewName($view),
            ['formData'      => $formData,
             'data'          => _Convert::toArray($tasks),
             'sourceTitle'   => 'Table = Tasks',
             'state'         => $data['state'] ?? null,
             'role'          => $data['role'] ?? null,
             'route'         => request()->route()->getName(),
             'btnAction'     => self::$prefix  . '.' . __FUNCTION__,
            ]);
    }

    public function userSummary()
    {
        if (!AccessController::hasAccess(self::$accessLevel))
        {
            Session::flash('error', 'You have no access to this transaction. Please try again.');
            Session::push('flash.old','error');
            return redirect()->back();
        }
        $request       = request();
        $expectedInput = ['userID',];
        $view = 'dashboard.'.self::$viewPath.'.tools.' . __FUNCTION__;

        $data = $request->all();

        $info = [];
        $userShareRooms = $userShareDocuments = $userLoginRoles = $userTransactionRoles = $userTransactions = [];
        $userData = new User;

        if (isset($data[reset($expectedInput)]))
        {

            $usrCtlr = new UserController();
            $userLoginRoles = $usrCtlr->loginRoles($data['userID']);

            $userTransactions = $usrCtlr->transactions($data['userID']);

            $userTransactionRoles = $usrCtlr->transactionRoles($data['userID']);

            foreach (ShareRoomCombine::getRoomsByUser($data['userID'], false) as $room)
            {
                $userShareRooms[] = ['Room'=>$room];
            }

            $userShareDocuments = ShareRoomCombine::getDocumentsByUser($data['userID'], false) ;

            //      ddd($userShareDocuments);
            /*
            ddd(['transactionList'=>$userTa, 'transactionRoles'=>$userTransactionRoles,
                'loginRoles'=>$userLoginRoles,
                'transactions'=>$userTransactions,]);
*/
            $formData = [];
            foreach ($expectedInput as $field)
            {
                if (!isset($data[$field])) continue;
                $formData[$field] = $data[$field];
            }
            $userData = User::find($data['userID']);
            //            $info = Task::where('State', $formData['state'])->where('TaskSets_Value', $formData['role'])->get();
        }
        else $formData = null;
        //dd($userData);
        return view(_LaravelTools::addVersionToViewName($view),
            ['formData'    => $formData,
             'data'        => ['shareRooms'       => $userShareRooms,
                               'shareDocuments'   => $userShareDocuments,
                               'loginRoles'       => $userLoginRoles,
                               'transactionRoles' => $userTransactionRoles,
                               'transactions'     => $userTransactions],
             'sourceTitle' => '',
             'userID'      => $data['userID'] ?? null,
             'userInfo'    => is_null($userData) ? 'Unknown' :
                 $userData->NameFirst ?? null . ' ' .
                                         $userData->NameLast ?? null .
                                                                ' - ' . $userData->email ?? null ,
             'route'       => request()->route()->getName(),
             'btnAction'   => self::$prefix . '.' . __FUNCTION__,
            ]);
    }

    public function deactivateUser()
    {
        if (!AccessController::hasAccess(self::$accessLevel))
        {
            Session::flash('error', 'You have no access to this transaction. Please try again.');
            Session::push('flash.old','error');
            return redirect()->back();
        }
        $request       = request();
        $expectedInput = ['userID', ];
        $view = 'dashboard.'.self::$viewPath.'.tools.' . __FUNCTION__;

        $data = $request->all();

        $info = [];

        $usrCtlr = new UserController();
        $userData = new User;
        $userList = $usrCtlr->getIndexValueArray();

        if (isset($data[reset($expectedInput)]))
        {

            $usrCtlr = new UserController();
            $userLoginRoles = $usrCtlr->loginRoles($data['userID']);

            $userTransactions = $usrCtlr->transactions($data['userID']);

            $userTransactionRoles = $usrCtlr->transactionRoles($data['userID']);

            foreach (ShareRoomCombine::getRoomsByUser($data['userID'], false) as $room)
            {
                $userShareRooms[] = ['Room'=>$room];
            }

            $userShareDocuments = ShareRoomCombine::getDocumentsByUser($data['userID'], false) ;

            //      ddd($userShareDocuments);
            /*
            ddd(['transactionList'=>$userTa, 'transactionRoles'=>$userTransactionRoles,
                'loginRoles'=>$userLoginRoles,
                'transactions'=>$userTransactions,]);
*/
            $formData = [];
            foreach ($expectedInput as $field)
            {
                if (!isset($data[$field])) continue;
                $formData[$field] = $data[$field];
            }
            $userData = User::find($data['userID']);
            //            $info = Task::where('State', $formData['state'])->where('TaskSets_Value', $formData['role'])->get();
        }
        else $formData = null;
        //dd($userData);
        return view(_LaravelTools::addVersionToViewName($view),
            ['formData'    => $formData,
             'data'        => [],
             'userList'    => $userList,
             'sourceTitle' => '',
             'userID'      => $data['userID'] ?? null,
             'userInfo'    => is_null($userData) ? 'Unknown' :
                 $userData->NameFirst ?? null . ' ' .
                                         $userData->NameLast ?? null .
                                                                ' - ' . $userData->email ?? null ,
             'route'       => request()->route()->getName(),
             'btnAction'   => self::$prefix . '.' . __FUNCTION__,
            ]);
    }
    public function switchCredentials()
    {
        if (!AccessController::hasAccess(self::$accessLevel))
        {
            Session::flash('error', 'You have no access to this transaction. Please try again.');
            Session::push('flash.old','error');
            return redirect()->back();
        }
        $request       = request();
        $expectedInput = ['userID', ];
        $view = 'dashboard.'.self::$viewPath.'.tools.' . __FUNCTION__;

        $data = $request->all();

        $info = [];

        $preSwitchCredentials = CredentialController::current();
        $newCredentials = null;

        $usrCtlr = new UserController();
        $userData = new User;
        $userList = $usrCtlr->getIndexValueArray();

        if (isset($data[reset($expectedInput)]))
        {
            CredentialController::switch($data['userID']);
            $newCredentials = CredentialController::current();

            $formData = [];
            foreach ($expectedInput as $field)
            {
                if (!isset($data[$field])) continue;
                $formData[$field] = $data[$field];
            }
            $userData = User::find($data['userID']);
            //            $info = Task::where('State', $formData['state'])->where('TaskSets_Value', $formData['role'])->get();
        }
        else $formData = null;
        //dd($userData);
        return view(_LaravelTools::addVersionToViewName($view),
            ['formData'    => $formData,
             'data'        => [],
             'userList'    => $userList,
             'preSwitch'   => $preSwitchCredentials,
             'newCredentials' => $newCredentials,
             'sourceTitle' => '',
             'userID'      => $data['userID'] ?? null,
             'userInfo'    => is_null($userData) ? 'Unknown' :
                 $userData->NameFirst ?? null . ' ' .
                                         $userData->NameLast ?? null .
                                                                ' - ' . $userData->email ?? null ,
             'route'       => request()->route()->getName(),
             'btnAction'   => self::$prefix . '.' . __FUNCTION__,
            ]);
    }
    public function saveEmailTemplate(Request $request)
    {
        $data = $request->all();

        $validationList = [
            'ID'                 => 'integer',
            'Category'           => 'required',
            'DisplayDescription' => 'required|min:10',
            'Subject'            => 'required',
            'Message'            => 'required',
            'Code'               => 'required',
            'ToRole'             => 'required',
            'FromRole'           => 'required',
            'CcRoles'            => 'required',
            'BccRoles'           => 'required',
        ];

        $validator = Validator::make($data,$validationList);
        if ($validator->fails())
        {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->getMessageBag()->toArray(),
            ], 400);
        }

        // ... Take the data from the form and make it fit the database
        if ($request->RentBack == 0)
        {
            $data['willRentBack']   = false;
            $data['RentBackLength'] = 0;
        }
        else
        {
            $data['willRentBack']   = true;
            $data['RentBackLength'] = $request->RentBack;
        }

        //... if the client role is an agent, set isClientAgent = true
        if (substr($request->ClientRole, -1) == 'a')
        {
            $data['isClientAgent'] = true;
        }
        else $data['isClientAgent'] = false;

        $transaction = new Transaction();
        $transaction->upsert($data);

        if (!isset($data['ID']))
        {
            $data['ID'] = $transaction->id;
        }
        //Log::debug(['&&', 'recalc dates'=>$data['recalculateDates'] ??  'not set', 'hasInspCon'=>$data['hasInspectionContingency'], __METHOD__=>__LINE__]);
        if (isset($data['recalculateDates']))
        {
            $recalculate = $data['recalculateDates'] === 'true' ? true : false;
            if ($recalculate)
            {
                $rv = MilestoneCombine::saveTransactionTimeline($data['ID'], $data['DateAcceptance'], $data['EscrowLength']);
                TaskCombine::resetTasks($data['ID']);
            }
        }

        return;

    }
}
