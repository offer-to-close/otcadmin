<?php

namespace App\Http\Controllers\OTC;

use Illuminate\Support\Facades\Auth;
use App\Combine\AccountCombine;
use App\Http\Controllers\AccessController;
use App\Http\Controllers\CredentialController;
use App\Http\Controllers\InvitationController;
use App\Library\otc\TableTools;
use App\Library\common\Utilities\_LaravelTools;
use App\Library\common\Utilities\DisplayTable;
use App\Models\Model_Parent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class StaffController extends Controller
{
    private static $accessLevel = 's';  // Staff Access
    private static $viewPath = 'staff';
    private static $prefix = 'staff';
    public function processToolMenu(Request $request, $path=null)
    {
        //       if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        if (!AccessController::hasAccess(self::$accessLevel))
        {
            Session::flash('error','You have no access to this transaction. Please try again.');
            Session::push('flash.old','error');
            return redirect()->back();
        }

        $data = $request->all();
        if (isset($data['btnAction']))
        {
            $method = str_replace(self::$prefix.'.', null, $data['btnAction']);
            return $this->{$method}();
        }
        if (!empty($path))
        {
            $method = str_replace(self::$prefix.'.', null, $path);
            return $this->{$method}();
        }
    }
    public function viewMenu(Request $request)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        if (!AccessController::hasAccess(self::$accessLevel))
        {
            Session::flash('error', 'You have no access to this transaction. Please try again.');
            Session::push('flash.old','error');
            return redirect()->back();
        }

        $view = 'dashboard.staff.menu';
        return (view(_LaravelTools::addVersionToViewName($view)));
    }
    public function summaries()
    {
        $view = 'dashboard.'.self::$viewPath.'.tools.' . __FUNCTION__;

        $userID = CredentialController::current()->ID();
        $transactions      =  AccountCombine::getTransactions( $userID)->toArray();
        $count = [];
        foreach ($transactions as $row)
        {
            if (!isset($count[$row->Status])) $count[$row->Status] = 0;
            ++$count[$row->Status];
        }

        $data = [];
        ksort($count);
        foreach ($count as $status=>$cnt)
        {
            $data[] = ['Transaction Status'=>$status, 'Count'=>$cnt];
        }

        $tableTransaction = DisplayTable::getBasic($data);
        return view(_LaravelTools::addVersionToViewName($view),
            ['statusTable' => $tableTransaction,
             'totalCount'=>count($transactions),
            ]);
    }
    public function reviewMembershipRequest()
    {
        $view = 'dashboard.'.self::$viewPath.'.tools.' . __FUNCTION__;

        $requests =  AccountCombine::getMemberRequests()->toArray();

        $excludeCol = ['TransactionSide', 'isApproved', 'isRejected', 'DecisionReason', 'DeciderUsers_ID', 'DateDecision',
                       'Decision_LogMail_ID', 'isTest', 'DateUpdated', 'deleted_at'];

        foreach($requests as $idx=>$row)
        {
            $r = [];
            if($row['NameFirst'] == $row['NameLast'])
            {
                $request = new InvitationController();
                $request->markRequestSpam($row['ID']);
                unset($requests[$idx]);
                continue;
            }
            foreach($row as $key=>$val)
            {
                if (in_array($key, $excludeCol)) continue;
                $r[$key] = $val;
            }
            $r['action']  = '<a href="'.route('staff.approveRequest', ['memReqID'=>$row['ID']]).'"><i class="far fa-thumbs-up"></i></a>';
            $r['action'] .= '&nbsp;&nbsp;' . '<a href="'.route('staff.rejectRequestReason', ['memReqID'=>$row['ID']]).'"><i class="far fa-thumbs-down"></i></a>';
            $r['action'] .= '&nbsp;' . '<i style="cursor:pointer;" class="fas fa-ban mark_spam" data-id="'.$r['ID'].'"></i>';
            $requests[$idx] = $r;
        }

        $tableTransaction = DisplayTable::getBasic($requests, ['excludeColumns'=>$excludeCol]);

        return view(_LaravelTools::addVersionToViewName($view),
            ['statusTable' => $tableTransaction,
             'totalCount'=>count($requests),
            ]);
    }
    public function reviewContactRequest()
    {
        $view = 'dashboard.'.self::$viewPath.'.tools.' . __FUNCTION__;
        Model_Parent::dataModel();
        $requests =  AccountCombine::getContactRequests()->toArray();

        krsort($requests);

        $excludeCol = ['DateClosed',
                       'DateCreated', 'Notes', 'Status', 'isTest', 'DateUpdated', 'deleted_at'];
        $renameCol = ['NameFirst'=>'First Name', 'NameLast'=>'Last Name', ];
        $booleanCol = ['startedEngagement', 'isClosed'];

        foreach($requests as $idx=>$row)
        {
            $r = [];
            foreach($row as $key=>$val)
            {
                if (in_array($key, $excludeCol)) continue;
                $r[$key] = $val;
            }
            //            $r['action']  = '<a href="'.route('staff.approveRequest', ['memReqID'=>$row['ID']]).'"><i class="far fa-thumbs-up"></i></a>';
            //            $r['action'] .= '&nbsp;' . '<i class="far fa-thumbs-down"></i>';
            $requests[$idx] = $r;
        }

        //        $tableTransaction = DisplayTools::editTable($requests,
        $tableTransaction = TableTools::table($requests,
            false,
            ['excludeColumns' => $excludeCol,
             'renameColumns'  => $renameCol,
             'booleanColumns' => $booleanCol,
             'showEditButton' => false,
            ]);

        return view(_LaravelTools::addVersionToViewName($view),
            ['statusTable' => $tableTransaction,
             'totalCount'=>count($requests),
            ]);
    }
}
