<?php

namespace App\Http\Controllers\Payments;

use App\Library\common\Utilities\_LaravelTools;
use App\Library\payments\StripeService;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Stripe\Charge;
use Stripe\Stripe;

class PaymentController extends Controller
{
    private static $service;

    public static function charge($service,$chargeType,$payload)
    {
        $services = ['stripe', ];
        if (!in_array($service, $services))
        {
            return self::jsonResponse(false, null, ['error'=>'Service Not Valid','service'=>$service, 'chargeType'=>$chargeType,
                                                    'payload'=>$payload]);
        }

        if ($service == 'stripe')
        {
            $stripe = self::StripeFactory();
            $rv = $stripe->submitCommand('charge', array_merge(['chargeType'=>$chargeType], $payload));
        }
    }
    public function checkoutSuccess()
    {

    }
    public function checkoutCancel()
    {

    }
/* *********************************************************************************************************
         ____    _            _
        / ___|  | |_   _ __  (_)  _ __     ___
        \___ \  | __| | '__| | | | '_ \   / _ \
        ___) |  | |_  | |    | | | |_) | |  __/
        |____/   \__| |_|    |_| | .__/   \___|
                                 |_|
 **********************************************************************************************************/
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripe()
    {
        $stripe = PaymentController::stripeFactory();
        $stripeSession= $stripe->createCheckoutSession();

//        Log::debug(['stripe session id'=>$stripeSession->id , __METHOD__=>__LINE__]);
        return view(_LaravelTools::addVersionToViewName('payment.subscribe'), ['stripeSession'=>$stripeSession, 'amt'=>91,]);
    }
    public static function stripeFactory() :StripeService
    {
        $serviceName = 'stripe';
        if (isset(self::$service[$serviceName])) return self::$service[$serviceName];
        self::$service[$serviceName] = new StripeService();
        return self::$service[$serviceName];
    }

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripePost(Request $request)
    {
        if (isServerLive()) $apiKey = config('payment.stripe.secretKey.live');
        else $apiKey = config('payment.stripe.secretKey.test');

        try
        {
            Stripe::setApiKey($apiKey);
            Charge::create ([
                "amount" => $request->amount * 100,
                "currency" => "usd",
                "source" => $request->stripeToken,
                "description" => "Test payment from otcAdmin.com."
            ]);
        }
        catch (\Stripe\Exception\CardException $e)
        {
            // Since it's a decline, \Stripe\Exception\CardException will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $errText[] = 'Status is: ' . $e->getHttpStatus();
            $errText[] = 'Type is: ' . $e->getError()->type;
            $errText[] = 'Code is: ' . $e->getError()->code;
            // param is '' in this case
            $errText[] = 'Param is: ' . $e->getError()->param;
            $errText[] = 'Message is: ' . $e->getError()->message;
            Log::notice(['Stripe Exception'=>$errText, __METHOD__=>__LINE__]);
        }
        catch (\Stripe\Exception\RateLimitException $e)
        {
            // Too many requests made to the API too quickly
        }
        catch (\Stripe\Exception\InvalidRequestException $e)
        {
            // Invalid parameters were supplied to Stripe's API
        }
        catch (\Stripe\Exception\AuthenticationException $e)
        {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
        }
        catch (\Stripe\Exception\ApiConnectionException $e)
        {
            // Network communication with Stripe failed
        }
        catch (\Stripe\Exception\ApiErrorException $e)
        {
            // Display a very generic error to the user, and maybe send
            // yourself an email
        }
        catch (Exception $e)
        {
            // Something else happened, completely unrelated to Stripe
        }

        Session::flash('success', 'Payment successful!');

        return back();
    }
    public function stripeWebhook (Request $request)
    {

        Log::debug(['request'=>$request,__METHOD__=>__LINE__]);
        $event = $request->getParsedBody();
// Parse the message body (and check the signature if possible)
        $webhookSecret = getenv('STRIPE_WEBHOOK_SECRET');
        if ($webhookSecret) {
            try {
                $event = \Stripe\Webhook::constructEvent(
                    $request->getBody(),
                    $request->getHeaderLine('stripe-signature'),
                    $webhookSecret
                );
            } catch (\Exception $e) {
                return $response->withJson([ 'error' => $e->getMessage() ])->withStatus(403);
            }
        } else {
            $event = $request->getParsedBody();
        }
        $type = $event['type'];
        $object = $event['data']['object'];

        if($type == 'checkout.session.completed') {
            $logger->info('🔔  Payment succeeded! ');
        }

        return $response->withJson([ 'status' => 'success' ])->withStatus(200);

    }
/* *********************************************************************************************************
          _   _   _     _   _   _   _     _
         | | | | | |_  (_) | | (_) | |_  (_)   ___   ___
         | | | | | __| | | | | | | | __| | |  / _ \ / __|
         | |_| | | |_  | | | | | | | |_  | | |  __/ \__ \
          \___/   \__| |_| |_| |_|  \__| |_|  \___| |___/

**********************************************************************************************************/

    private static function jsonResponse($isSuccessful, $returnValue, $payload=[])
    {
        return json_encode([
            'isSuccessful' => $isSuccessful,
            'returnValue' => $returnValue,
            'payload' => $payload,
        ]);
    }
    public static function testStripeFactory()
    {
        $stripe = self::StripeFactory();
        $stripe->_test();
    }
}
