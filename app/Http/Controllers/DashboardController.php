<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function mainDisplay(Request $request)
    {
        $currentService = session('service');
        $services = config('otcCommon.constants.OtcServices');
        return view('1a.dashboard.main',
            [
                'currentService' => $currentService,
                'services'       => $services,
                ]);
    }
    public function switchToOtc()
    {
        return $this->switchService('otc');
    }
    public function switchToOh()
    {
        return $this->switchService('oh');
    }
    public function switchToOffer()
    {
        return $this->switchService('offer');
    }

    public function switchService($serviceCode)
    {
        $dbServices = config('otcServices.dbServices');
        if (isset($dbServices[$serviceCode]))
        {
            $route = '1a.'.$serviceCode.'.main';
            session(['dbServiceCode'  => $dbServices[$serviceCode]['dbConnectionSuffix'],
                     'serviceDisplay' => $dbServices[$serviceCode]['displayName'],
                     'serviceCode'    => $serviceCode,
                     'service'        => $serviceCode]);
            return view($route);
        }
        return false;
    }
    public function offerToClose()
    {
        return redirect()->away('https://www.OfferToClose.com');
    }
}
