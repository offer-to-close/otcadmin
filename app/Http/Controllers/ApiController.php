<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function contants(Request $request, $key, $constant=null)
    {
        if (empty($constant)) return json_encode(config('otcCommon'));
        return json_encode(config('otcCommon.'.$constant));
    }
}
