<?php

namespace App\Scopes;

use App\Library\common\Utilities\_Variables;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class NoTestScope implements Scope
{
/**
 * Apply the scope to a given Eloquent query builder.
 * In this case the scope limits the query to records where isTest value is false.
 *
 * @param  \Illuminate\Database\Eloquent\Builder  $builder
 * @param  \Illuminate\Database\Eloquent\Model  $model
 * @return void
 */
    public function apply(Builder $builder, Model $model)
    {
      if (_Variables::getObjectName($builder->getModel()))
      {
//          dd(['builder'=>$builder, 'model'=>$model]);
      }
      $builder->where('isTest', '=', false)->orWhereNull('isTest');
    }
}