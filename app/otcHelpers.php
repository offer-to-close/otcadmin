<?php

use Illuminate\Support\Debug\HtmlDumper;
use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\CliDumper;
use Illuminate\Support\Facades\Log;

/**
 * @param     $variable
 * @param int $depth
 * @param int $stringLength
 */
function ddd($variable, $depth = -1, $stringLength = 50)
{
    $server = 'Server: '.getServer();

    if ($depth == '**')
    {
        $depth = -1;
        $stringLength = '*';
    }

    if ($depth == '*') $depth = -1;

    if (!isServerStage() && !isServerLocal())
    {
        if (!is_array($variable)) $variable = [$variable];
        $variable = array_merge(['&&'=>'Redirect from ddd', ], $variable);
        //        Log::info($variable);
        die(1);
    }

    $cloner = new VarCloner();
    if ($stringLength != '*' && is_numeric($stringLength)) $cloner->setMaxString($stringLength);

    $dumper = 'cli' === PHP_SAPI ? new CliDumper() : new HtmlDumper();

    $dumper->dump($cloner->cloneVar($server));
    $dumper->dump($cloner->cloneVar($variable)->withMaxDepth($depth));

    die(1);
}

/**
 * @return bool
 */
function isServerLive()
{
    if (strtolower(env('APP_ENV')) != 'live' &&
        strtolower(env('APP_ENV')) != 'prod' &&
        strtolower(env('APP_ENV')) != 'production')
    {
        return false;
    }
    return true;
}

/**
 * @return bool
 */
function isServerLocal($who=null)
{
    if (strtolower(env('APP_ENV')) != 'local') return false;
    if (!empty($who) && env('DEV') != $who) return false;
    return true;
}

/**
 * @return bool
 */
function isServerTest($who=null)
{
    if (strtolower(env('APP_ENV')) != 'test') return false;
    if (!empty($who) && env('DEV') != $who) return false;
    return true;
}

/**
 * @return bool
 */
function isServerStage($who=null)
{
    if (strtolower(env('APP_ENV')) != 'stage' &&
        strtolower(env('APP_ENV')) != 'staging')
    {
        return false;
    }
    if (!empty($who) && env('DEV') != $who) return false;
    return true;
}

/**
 * @return mixed
 */
function getServer()
{
    return env('APP_ENV');
}

/**
 * @param      $path
 * @param bool $overwrite
 *
 * @return bool
 */
function canCreateFile($path, $overwrite=true)
{
    if (!$overwrite && is_file($path)) return false;

    if (!$overwrite) $fh = fopen($path, 'x');
    else $fh = fopen($path, 'w');

    if (!$fh) return false;
    fclose($fh);
    return unlink($path);
}

/**
 * @param        $to
 * @param        $body
 * @param string $subject
 * @param string $from
 * @param bool   $logIt
 *
 * @return bool
 */
function quickEmail( $to, $body, $subject='*', $from='*', $logIt=false)
{
    if (empty($to) || empty($body)) return false;
    $subject = ($subject == '*') ? 'A message from Offer To Close': $subject;
    $from = ($from == '*') ? config('mail.from.address') : $from;

    Mail::raw($body, function ($message) use ($body, $subject, $to, $from) {
        $message->to($to)->from($from)->subject->$subject;
    });
    if ($logIt)
    {

    }

    return true;
}
function formatPhoneNumber($input, $delimiter='-')
{
    Log::debug(['input'=>$input, __METHOD__=>__LINE__]);
    if (empty($input)) return null;
    $ay = str_split($input);
    $clean = null;
    foreach ($ay as $char)
    {
        if (is_numeric($char) || strtolower($char) == 'x') $clean .= $char;
        Log::debug($char.': '. $clean);
    }
    $phone = null;
    $ext = null;

    Log::debug(['clean'=>$clean, __METHOD__=>__LINE__]);

    if (stripos($clean, 'x') !== false)
    {
        list($phone, $ext) = explode('x', strtolower($clean));
    }
    else $phone = $clean;

    if (strlen($phone) == 10 ) $phone = substr($phone, 0, 3) . $delimiter
                                        . substr($phone, 3,3) . $delimiter
                                        . substr($phone, -4);

    Log::debug(['phone'=>$phone, __METHOD__=>__LINE__]);
    return $phone . (empty($ext) ? null : ' x' . $ext);
}