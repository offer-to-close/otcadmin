<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Model_Parent;

class OtcUser extends Model_Parent
{
    protected $table = 'OtcUsers';
    protected $connection = 'mysql-admin';
}
