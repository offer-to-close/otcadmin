<?php

namespace App\Models;

use App\Models\Model_Parent;
use Illuminate\Database\Eloquent\SoftDeletes;

class lu_Table_Parent extends Model_Parent
{
    use SoftDeletes;
    const SORT_BY_NONE = null;
    const SORT_BY_VALUE = 'value';
    const SORT_BY_DISPLAY = 'display';
    const SORT_BY_ORDER = 'order';

    public static $valueTable = [];

    public function getSorted($sortType = null)
    {
        switch ($sortType)
        {
            case LookupTable::SORT_BY_VALUE:
                break;
            case LookupTable::SORT_BY_DISPLAY:
                break;
            case LookupTable::SORT_BY_ORDER:
                break;
            case LookupTable::SORT_BY_NONE:
            default:
                break;
        }
    }

    public static function getDisplay($value = null, $displayFormat = false)
    {
        $rv = self::where('Value', $value)->select('Display')->get();
        if ($rv && $rv->count() == 1)
        {
            $rv = $rv->first()->Display;
        }
        else $rv = false;
        if ($displayFormat) $rv = title_case(str_replace('_', ' ', $rv));
        return $rv;
    }

    public static function getValueList($sortBy = self::SORT_BY_ORDER, $sortOrder = SORT_ASC, $excludeZeros = false)
    {
        $sortByList = [self::SORT_BY_ORDER, self::SORT_BY_VALUE, self::SORT_BY_DISPLAY,];
        if (!in_array($sortBy, $sortByList)) return false;

        $query = static::select('Value');
        if ($sortOrder == SORT_ASC) $query = $query->orderBy(title_case($sortBy), SORT_ASC);
        if ($sortOrder == SORT_DESC) $query = $query->orderBy(title_case($sortBy), SORT_DESC);
        if ($excludeZeros) $query = $query->where('Order', '!=', 0);

        return $query->get();
    }

    public static function loadValueTable()
    {
        self::$valueTable = [];

        foreach (static::select('Value', 'Display', 'ValueInt', 'isTest')->get() as $row)
        {
            if ($row->isTest > 0) continue;
            self::$valueTable[$row->Value] = ['value' => $row->Value, 'display' => $row->Display, 'valueInt' => $row->ValueInt,];
        }
    }

    public static function valueDisplayArray($sortBy = self::SORT_BY_ORDER, $sortOrder = SORT_ASC, $excludeZeros = false)
    {
        $sortByList = [self::SORT_BY_ORDER, self::SORT_BY_VALUE, self::SORT_BY_DISPLAY,];
        if (!in_array($sortBy, $sortByList)) return false;

        $query = static::select('Value', 'Display');
        if ($sortOrder == SORT_ASC) $query = $query->orderBy(title_case($sortBy), SORT_ASC);
        if ($sortOrder == SORT_DESC) $query = $query->orderBy(title_case($sortBy), SORT_DESC);
        if ($excludeZeros) $query = $query->where('Order', '!=', 0);

        return $query->get()->toArray();
    }

    public static function displayValueintArray($sortBy = self::SORT_BY_ORDER, $sortOrder = SORT_ASC, $excludeZeros = false)
    {
        $sortByList = [self::SORT_BY_ORDER, self::SORT_BY_VALUE, self::SORT_BY_DISPLAY,];
        if (!in_array($sortBy, $sortByList)) return false;

        $query = static::select('Display', 'ValueInt');
        if ($sortOrder == SORT_ASC) $query = $query->orderBy(title_case($sortBy), SORT_ASC);
        if ($sortOrder == SORT_DESC) $query = $query->orderBy(title_case($sortBy), SORT_DESC);
        if ($excludeZeros) $query = $query->where('Order', '!=', 0);

        return $query->get()->toArray();
    }
}
