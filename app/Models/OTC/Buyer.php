<?php

namespace App\Models\OTC;

use App\Models\Model_Parent;
use Illuminate\Database\Eloquent\Model;

class Buyer extends Model_Parent
{
    protected $table = 'Buyers';
    protected $connection = 'mysql-otc';
}
