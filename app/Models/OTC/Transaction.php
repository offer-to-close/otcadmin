<?php

namespace App\Models\OTC;

use App\Models\Model_Parent;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model_Parent
{
    protected $table = 'Transactions';
    protected $connection = 'mysql-otc';
}
