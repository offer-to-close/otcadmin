<?php

namespace App\Models\OTC;

use App\Models\Model_Parent;
use Illuminate\Database\Eloquent\Model;

class TransactionCoordinator extends Model_Parent
{
    protected $table = 'TransactionCoordinators';
    protected $connection = 'mysql-otc';
}
