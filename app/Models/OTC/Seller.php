<?php

namespace App\Models\OTC;

use App\Models\Model_Parent;
use Illuminate\Database\Eloquent\Model;

class Seller extends Model_Parent
{
    protected $table = 'Sellers';
    protected $connection = 'mysql-otc';
}
