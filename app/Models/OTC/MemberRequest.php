<?php

namespace App\Models\OTC;

use App\Models\Model_Parent;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class MemberRequest extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'MemberRequests';
    protected $connection = 'mysql-otc';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
}
