<?php

namespace App\Models\OTC;

use App\Models\Model_Parent;
use Illuminate\Database\Eloquent\Model;

class Title extends Model_Parent
{
    protected $table = 'Titles';
    protected $connection = 'mysql-otc';
}
