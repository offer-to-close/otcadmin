<?php

namespace App\Models\OTC;

use App\Models\Model_Parent;
use Illuminate\Database\Eloquent\Model;

class Escrow extends Model_Parent
{
    protected $table = 'Escrows';
    protected $connection = 'mysql-otc';
}
