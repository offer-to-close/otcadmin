<?php

namespace App\Models\OTC;

use App\Models\lu_Table_Parent;
use Illuminate\Database\Eloquent\Model;

class lu_UserRoles extends lu_Table_Parent
{
    protected $table = 'lu_UserRoles';
    protected $connection = 'mysql-otc';
}
