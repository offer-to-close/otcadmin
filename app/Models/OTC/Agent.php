<?php

namespace App\Models\OTC;

use App\Models\Model_Parent;
use Illuminate\Database\Eloquent\Model;

class Agent extends Model_Parent
{
    protected $table = 'Agents';
    protected $connection = 'mysql-otc';
}
