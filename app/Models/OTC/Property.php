<?php

namespace App\Models\OTC;

use App\Models\Model_Parent;
use Illuminate\Database\Eloquent\Model;

class Property extends Model_Parent
{
    protected $table = 'Properties';
    protected $connection = 'mysql-otc';
}
