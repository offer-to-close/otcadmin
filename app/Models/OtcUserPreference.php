<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OtcUserPreference extends Model
{
    use SoftDeletes;
    protected $table = 'OtcUserPreferences';
}
