<?php
/*
 * This set of functions is automatically loaded through the composer.json file
 * ...
  "autoload": {
        "files":[
            "app/HelperFunctions/otcHelpers.php"
        ],
 *
 */

use App\Combine\AccountCombine;
use App\Combine\TransactionCombine;
use App\Http\Controllers\CredentialController;
use Symfony\Component\VarDumper\Dumper\HtmlDumper;
use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\CliDumper;
use Illuminate\Support\Facades\Log;

/**
 * @param     $variable
 * @param int $depth
 * @param int $stringLength
 */
function ddd($variable, $depth = -1, $stringLength = 50)
{
    $server = 'Server: '.getServer();

    if ($depth == '**')
    {
        $depth = -1;
        $stringLength = '*';
    }

    if ($depth == '*') $depth = -1;

    if (!isServerStage() && !isServerLocal())
    {
        if (!is_array($variable)) $variable = [$variable];
        $variable = array_merge(['&&'=>'Redirect from ddd', ], $variable);
        //        Log::info($variable);
        die(1);
    }

    $cloner = new VarCloner();
    if ($stringLength != '*' && is_numeric($stringLength)) $cloner->setMaxString($stringLength);

    $dumper = 'cli' === PHP_SAPI ? new CliDumper() : new HtmlDumper();

    $dumper->dump($cloner->cloneVar($server));
    $dumper->dump($cloner->cloneVar($variable)->withMaxDepth($depth));

    die(1);
}

/**
 * @return bool
 */
function isServerLive()
{
    if (strtolower(env('APP_ENV')) != 'live' &&
        strtolower(env('APP_ENV')) != 'prod' &&
        strtolower(env('APP_ENV')) != 'production')
    {
        return false;
    }
    return true;
}

/**
 * @return bool
 */
function isServerLocal($who=null)
{
    if (strtolower(env('APP_ENV')) != 'local') return false;
    if (!empty($who) && env('DEV') != $who) return false;
    return true;
}

/**
 * @return bool
 */
function isServerTest($who=null)
{
    if (strtolower(env('APP_ENV')) != 'test' &&
        strtolower(env('APP_ENV')) != 'dev'  &&
        strtolower(env('APP_ENV')) != 'develop') return false;
    if (!empty($who) && env('DEV') != $who) return false;
    return true;
}

/**
 * @return bool
 */
function isServerStage($who=null)
{
    if (strtolower(env('APP_ENV')) != 'stage' &&
        strtolower(env('APP_ENV')) != 'staging')
    {
        return false;
    }
    if (!empty($who) && env('DEV') != $who) return false;
    return true;
}

function isServerUnitTest($who=null)
{
    if (strtolower(env('APP_ENV')) != 'testing' &&
        strtolower(env('APP_ENV')) != 'unittest' &&
        strtolower(env('APP_ENV')) != 'unit')
    {
        return false;
    }
    if (!empty($who) && env('DEV') != $who) return false;
    return true;
}

/**
 * @return mixed
 */
function getServer()
{
    return env('APP_ENV');
}

/**
 * @param      $path
 * @param bool $overwrite
 *
 * @return bool
 */
function canCreateFile($path, $overwrite=true)
{
    if (!$overwrite && is_file($path)) return false;

    if (!$overwrite) $fh = fopen($path, 'x');
    else $fh = fopen($path, 'w');

    if (!$fh) return false;
    fclose($fh);
    return unlink($path);
}

/**
 * @param        $to
 * @param        $body
 * @param string $subject
 * @param string $from
 * @param bool   $logIt
 *
 * @return bool
 */
function quickEmail( $to, $body, $subject='*', $from='*', $logIt=false)
{
    if (empty($to) || empty($body)) return false;
    $subject = ($subject == '*') ? 'A message from Offer To Close': $subject;
    $from = ($from == '*') ? config('mail.from.address') : $from;

    Mail::raw($body, function ($message) use ($body, $subject, $to, $from) {
        $message->to($to)->from($from)->subject->$subject;
    });
    if ($logIt)
    {

    }

    return true;
}
function formatPhoneNumber($input, $delimiter='-')
{
    Log::debug(['input'=>$input, __METHOD__=>__LINE__]);
    if (empty($input)) return null;
    $ay = str_split($input);
    $clean = null;
    foreach ($ay as $char)
    {
        if (is_numeric($char) || strtolower($char) == 'x') $clean .= $char;
        Log::debug($char.': '. $clean);
    }
    $phone = null;
    $ext = null;

    Log::debug(['clean'=>$clean, __METHOD__=>__LINE__]);

    if (stripos($clean, 'x') !== false)
    {
        list($phone, $ext) = explode('x', strtolower($clean));
    }
    else $phone = $clean;

    if (strlen($phone) == 10 ) $phone = substr($phone, 0, 3) . $delimiter
                                        . substr($phone, 3,3) . $delimiter
                                        . substr($phone, -4);

    Log::debug(['phone'=>$phone, __METHOD__=>__LINE__]);
    return $phone . (empty($ext) ? null : ' x' . $ext);
}
function setTransactionListToSession()
{
    $role = \session('userRole');
    $userID = CredentialController::current()->ID();
    $roleID = AccountCombine::getRoleID($role,$userID);
    $roleID = $roleID->first();
    $roleID = is_array($roleID) ? $roleID['ID'] : $roleID->ID;
    $transactions = TransactionCombine::transactionList($role, $roleID, $userID);
    session()->put('transactionList__JSON', json_encode(array_values($transactions['success']['info']['collection']->toArray())));
}

function cleanQuestionnaireDataReceived($rawData)
{
    $rawData = $rawData['answers'];
    $data = [];
    foreach ($rawData as $array) {
        if (!empty($array)) {
            foreach ($array as $index => $values) {
                $data[$index] = (object)$values;
            }
        }
    }

    return $data;
}


function curlPost($url, $data, $credentials)
{
    return otcCurl($url, $data, $credentials, 'post');
}
function otcCurl($url, $data, $credentials, $method='post')
{
    $factory        = new BuilderFactory();
    $builder        = $factory->create();

    $password       = $credentials['password'];   // '<super secret password>';
    $username       = $credentials['username']; //'foo@bar.ru';

    //begin building the request
    $builder->asJson();
    $builder->onTheUrl($url);
    $builder->withTheData($data);
    $builder->withTheOption(new SetBasicAuthentication());
    $builder->withTheOption(new SetUsernameAndPassword($username, $password));
    if ($method == 'post') $builder->usePost();
    elseif ($method == 'get') $builder->useGet();
    elseif ($method == 'put') $builder->usePut();
    //end building the request

    $request = $builder->andFetchTheResponse();

    if (isServerLocal()) Log::debug([
        'Curl Request',
        'url'=>$url,
        'data'=>$data,
        'response'=>$request,
        __METHOD__=>__LINE__
    ]);

    return $request;
    return response($request);
}

/**
 * @param null $ip
 *
 * @return bool|string
 * @throws \Exception
 *
 *
 *   http://ip-api.com/json/<ip> returns a JSON with the following format:
 *          {
 *               "as": "AS20001 Charter Communications Inc",
 *               "city": "Woodland Hills",
 *               "country": "United States",
 *               "countryCode": "US",
 *               "isp": "Spectrum",
 *               "lat": 34.1765,
 *               "lon": -118.614,
 *               "org": "Charter Communications",
 *               "query": "194.04.90.10",          // This is the <ip> used in the endpoint
 *               "region": "CA",
 *               "regionName": "California",
 *               "status": "success",
 *               "timezone": "America/Los_Angeles",
 *               "zip": "91367"
 *          }
 */
function userTimeZone($ip=null)
{
    if (empty($ip)) $ip = request()->ip();
    if ($ip == '127.0.0.1') return (new DateTime())->getTimezone()->getName();

    $url  = 'http://ip-api.com/json/' . $ip;
    $tz   = file_get_contents($url);
    $time = json_decode($tz, true);

    if (isset($time['timezone'])) return $time['timezone'];
    return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * findSet
 *
 * Finds the first set of string that is properly nested using a matching pair of delimiters. The
 * delimiters are () {} <> & {}
 *
 * @param string $string      String in which to find set
 * @param string $chrOpen     The opening character of the set delimiter '('|'['|'<'|'{'
 * @param bool   $includeEnds If TRUE, the returned string will include the leading and ending delimeters
 * @param mixed  $details     Returns the various values that describe where the set is
 *                            Typical use:
 *                                   while ($v = findSet($subject, '{', true, $details))
 *                                   {
 *                                      $vars[] = $v;
 *                                      $subject = substr($subject, $details['setEnd']+1);
 *                                   }
 *                            The above code extracts all the text from all the sets of curly braces within the $subject variable.
 *
 * @document details The indices of the array that gets assigned to $details
 *
 * @return string|bool Value within the set or FALSE
 *
 * @author   Marc Zev
 *
 * @version  2.0.0.2015
 */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function findSet($string, $chrOpen = '(', $includeEnds = true, &$details = [])
{
    $details = ['originalLength' => false, 'setStart' => false, 'setEnd' => false, 'setLength' => false, 'includeEnds' => $includeEnds];
    $chrSets = ['(' => ')', '[' => ']', '<' => '>', '{' => '}', '`' => '`'];
    $pos     = -1;
    $cnt     = 0;

    $chrs = str_split($string, 1);

    $details['originalLength'] = count($chrs);

    $final = null;
    $store = false;
    foreach ($chrs as $c)
    {
        ++$pos;

        if ($c == $chrOpen)
        {
            if ($cnt == 0)
            {
                $store = true;
                if ($includeEnds)
                {
                    $final               .= $chrOpen;
                    $details['setStart'] = $pos;
                }
                else $details['setStart'] = $pos + 1;

            }
            else  $final .= $c;

            ++$cnt;
            continue;
        }
        else
        {
            if ($c == $chrSets[$chrOpen])
            {
                --$cnt;
                if ($cnt == 0)
                {
                    $store = false;
                    if ($includeEnds)
                    {
                        $final             .= $c;
                        $details['setEnd'] = $pos;
                    }
                    else $details['setEnd'] = $pos - 1;
                    break;
                }
                else $final .= $c;
                continue;
            }
            else if ($store) $final .= $c;
        }
    }

    $details['setLength'] = strlen($final);

    if ($cnt == 0)
    {
        return $final;
    }
    else return false;
}

function getStateByIp($ip=null)
{
    if (empty($ip)) $ip = Request::ip();
    if (isServerLocal() && $ip == '127.0.0.1') $ip = env('testIP');
    $endpoint = str_replace('<ip>', $ip, env('IPSTACK_TEMPLATE'));
    $json = file_get_contents($endpoint);
    $ay = json_decode($json, true);
    return $ay['region_code'] ?? null;
}