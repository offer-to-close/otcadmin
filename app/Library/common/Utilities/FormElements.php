<?php

namespace App\Library\common\Utilities;

class FormElements
{
// properties
    public $objName = "Access";

    private $userLists = array();
    private $listA = array("A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
//////// __construct()
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function __construct()
    {
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
//////// buildDateElement()
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function buildDateElement($label = "", &$element)
    {
        $isDate = true;
        $rv     = $this->buildTextElement($label, $element, $isDate);

        return ($rv);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
//////// buildDateScript()
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static public function buildDateScript($fldName, $format = 'y-m-d')
    {
        $convertFormat = array('y-m-d' => 'yy-m-d', 'm/d/y' => 'm/d/yy');
        if (isset($convertFormat[$format]))
        {
            $format = $convertFormat[$format];
        }
        else $format = $convertFormat[reset($convertFormat)];

        $rv = "<script>" . " $(function() {\$( \"#{$fldName}\" ).datepicker({ dateFormat: \"{$format}\" }); }); " . " </script>" . "\n";
        return ($rv);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
//////// buildElement()
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function buildElement($label = "", &$element)
    {
        if (!is_array($element)) return (false);
        if (!isset($element['type'])) return (false);

        switch (strtolower($element['element']))
        {
            case ('userlist'):
                $lookup         = $element['lookup'];
                $list           = UserList::getList($lookup);
                $tmp            = $element;
                $tmp['options'] = $list;
                $rv             = $this->buildDropdownElement($label, $tmp);
                break;

            case ('text'):
                $rv = $this->buildTextElement($label, $element);
                break;

            case ('textarea'):
                $rv = $this->buildTextareaElement($label, $element);
                break;

            case ('radio'):
                $rv = $this->buildRadioElement($label, $element);
                break;

            case ('checkbox'):
                $lookup       = $element['lookup'];
                $list         = UserList::getList($lookup);
                $tmp          = $element;
                $tmp['value'] = $list;

                $rv = $this->buildCheckboxElement($label, $tmp);
                break;

            case ('dropdown'):
                $rv = $this->buildDropdownElement($label, $element);
                break;

            case ('fillinlist'):
                $rv = $this->buildFillinListElement($label, $element);
                break;

            case ('hr'):
                $rv = $this->buildHrElement();
                break;

            case ('date'):
                $rv = $this->buildDateElement($label, $element);
                break;

            default:
                echo "<br/>Storm it! I have no idea what you mean by `{$element['type']}`.<br/>" . "\n";
                $rv = null;
                break;
        }
        return ($rv);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
//////// buildHiddenElement()
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function buildHiddenElement($name, $value)
    {
        $a = "<input type='hidden' name='{$name}' value='{$value}' /> ";
        return ($a);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
//////// buildTextElement()
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function buildTextElement($label = "", &$element, $isDate = false)
    {
        $classes = array();

        $vars = $element['+vars'];

        $a   = $label . ((!empty($label)) ? ": " : null) . "<input ";
        $suf = null;

        if (!isset($element['id']) || empty($element['id']))
        {
            if (isset($element['name']))
            {
                $element['id'] = $element['name'];
            }
            else  $element['id'] = $element['element'] . "-" . _Get::randomNumber(5);
        }

        if (isset($element['class']) && !empty($element['class']))
        {
            $classes = explode(" ", $element['class']);
        }

        if (isset($element['editable']) && $element['editable'] == 'false')
        {
            $hide = $this->buildHiddenElement($element['name'], (isset($element['value']) ? $element['value'] : null));
        }
        else $hide = null;

        if (!empty($hide)) $element['name'] .= "-readonly";

        $req = null;
        foreach ($element as $key => $val)
        {
            if (substr($key, 0, 1) == '+') continue;
            if (array_search('*' . $key, $vars) !== false)
            {
                switch ($key)
                {
                    case ('editable'):
                        if (strtolower($val) == "false")
                        {
                            $a         .= " READONLY ";
                            $classes[] = 'readonly';
                        }
                        break;

                    case ('required'):
                        if (strtolower($val) == "true")
                        {
                            $a         .= " REQUIRED ";
                            $req       = "&nbsp;*";
                            $classes[] = 'required';
                        }
                        break;

                    default:
                        continue;
                }
                continue;
            }
            else
            {
                if ($key == 'suffix') $suf = $val;
                $a .= $key . "=\"" . $val . "\" ";
            }
        }

        if (count($classes) > 0)
        {
            $tagClass = " class='" . implode(" ", $classes) . "' ";
        }
        else $tagClass = null;

        if (!empty($tagClass)) $a .= $tagClass;

        if ($isDate)
        {
            $dat = $this->buildDateScript($element['id']);
        }
        else $dat = null;

        $a .= /*explode(" ", $extras) .*/
            " /> " . $req . $hide . $suf . $dat;
        return ($a);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
//////// buildTextareaElement()
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function buildTextareaElement($label = "", &$element)
    {
        $default = (isset($element['default'])) ? $element['default'] : null;
        $a       = $label . ": " . "<textarea ";
        $suf     = null;

        foreach ($element as $key => $val)
        {
            if ($key == 'type') continue;
            if ($key == 'label') continue;
            if ($key == 'value') continue;
            if ($key == 'suffix') $suf = $val;
            $a .= $key . "=\"" . $val . "\" ";
        }
        $a .= "> " . $suf;
        $a .= $default . "</textarea>";

        return ($a);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
//////// buildRadioElement()
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function buildRadioElement($label = "", &$element)
    {
        $default = (isset($element['default'])) ? $element['default'] : null;

        $a   = $label . ": ";
        $suf = null;

        $values      = $element['value'];
        $useKeyLabel = false;

        if (!is_array($element['value']))
        {
            $ul     = $this->getUserList(strtolower($element['value']));
            $values = array();
            if (isset($ul['numbering'])) $useKeyLabel = $ul['numbering'];
            if (!$ul['numberMask'] || substr_count($ul['numberMask'], "#") == 0) $ul['numberMask'] = "#";

            $i = 0;
            foreach ($ul['list'] as $item)
            {
                switch ($ul['numberType'])
                {
                    case ("1"):
                        $key = str_replace("#", $i++, $ul['numberMask']);
                        break;

                    case ("a"):
                        $key = str_replace("#", strtolower($this->listA[$i++]), $ul['numberMask']);
                        break;

                    case ("A"):
                    default:
                        $key = str_replace("#", strtoupper($this->listA[$i++]), $ul['numberMask']);
                        break;
                }
                $values[$key] = ($useKeyLabel) ? $key . " " . $item : $item;
            }
        }

        foreach ($values as $btn)
        {
            $a     .= " <input ";
            $final = "";
            foreach ($element as $key => $val)
            {
                if ($key == 'displayFormat') $final = "<br/>";
                if ($key == 'value') $val = $btn;
                if ($key == 'suffix') $suf = $val;

                $a .= $key . "=\"" . $val . "\" ";
            }
            if ($default == $btn) $a .= " checked ";
            $a .= ">" . $btn . $final . "\n";
            echo $a;
            $a = null;
        }
        return (" " . $suf);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
//////// buildCheckboxElement()
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function buildCheckboxElement($label = "", &$element)
    {
        $ignore = array('label', 'lookup', 'element', 'required');

        $default = (isset($element['default'])) ? $element['default'] : null;

        if (!is_array($default)) $default = array($default);

        $a   = $label . ": ";
        $suf = null;

        if (isset($element['value']) && is_array($element['value'])) $element['value'] = array_map('trim', $element['value']);

        foreach ($element['value'] as $btn)
        {
            $a .= " <input type='checkbox' ";
            foreach ($element as $key => $val)
            {
                if (array_search($key, $ignore) !== false) continue;
                if ($key == 'value') $val = trim($btn);
                if ($key == 'name') $val = trim($element['name']) . '[]';
                if ($key == 'suffix') $suf = $val;

                $a .= $key . "=\"" . $val . "\" ";
            }
            if (array_search($btn, $default) !== false) $a .= " checked ";
            $a .= ">" . $btn . "\n";
        }
        return ($a . $suf);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
//////// buildDropdownElement()
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function buildDropdownElement($label = "", &$element)
    {
        $rv      = null;
        $default = (isset($element['default'])) ? $element['default'] : null;

        $a   = $label . ": " . "<select ";
        $suf = null;
        foreach ($element as $key => $val)
        {
            if ($key != 'options') $a .= $key . "=\"" . $val . "\" ";
            if ($key == 'suffix') $suf = $val;
        }
        $a  .= ">" . $suf . "\n";
        $rv .= $a;

        foreach ($element['options'] as $val => $dsp)
        {
            if ($default == $val)
            {
                $sel = " selected  ";
            }
            else $sel = "";

            $rv .= "<option {$sel} value='{$val}'>{$dsp}</option>" . "\n";
        }
        $rv .= "</select>" . $suf;

        return ($rv);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
//////// buildFillinListElement()
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function buildFillinListElement($label = "", $element)
    {

        $listLength = (isset($element['listLength'])) ? $element['listLength'] : 10;

        $element['name'] = trim($element['name']) . "[]";

        $element['type'] = "text";

        $rv = null;
        for ($i = 0; $i < $listLength; $i++)
        {
            if ($i == 0)
            {
                $l = $label;
            }
            else $l = null;
            $rv .= $this->buildTextElement($l, $element) . "\n";
        }

        return ($rv);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
//////// buildHrElement()
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function buildHrElement()
    {
        $a = "<hr/>";

        return ($a . "\n");
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
//////// getUserList($listName)
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function getUserList($listName)
    {

        if (!isset($this->userLists[$listName]))
        {
            return (false);
        }
        else return ($this->userLists[$listName]);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
//////// setUserList($listName, $list)
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function setUserList($listName, &$list)
    {
        $this->userLists[strtolower($listName)] = $list;
        return;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
//////// iconButton($img, $parm)
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static public function iconButton($img, &$parm, $alt = '', $width = '32px', $height = '32px')
    {
        $el = "<form class='button' style='display:inline;' method='post' action='" . '#' . "'>";
        if (!isset($parm)) $parm = array();
        foreach ($parm as $var => $val)
        {
            $el .= "<input type='hidden' name='{$var}' value='{$val}'/>";
        }
        $el .= "<button type='submit' style='width:{$width}; height:{$height};' class='btnImage'><img alt='$alt' style='width:{$width}; height:{$height};' src='{$img}'/></button>";
        $el .= "</form>";
        return ($el);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
//////// imageButton($img, $parm)
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static public function imageButton($img, &$parm, $alt = NULL, $width = '32px', $height = '32px')
    {

        if (isset($parm['*method']))
        {
            $method = $parm['*method'];
        }
        else $method = 'post';


        $el = "<form class='button' method='{$method}' action='" . '#' . "'>";
        if (is_null($alt))
        {
            $alt = $img;
        }
        if (!isset($parm)) $parm = array();
        foreach ($parm as $var => $val)
        {
            $el .= "<input type='hidden' name='{$var}' value='{$val}'/>";
        }
        $el .= "<button type='submit' style='width:{$width}; height:{$height};' class='btnImage'><img alt='$alt' style='width:{$width}; height:{$height};' src='{$img}'/></button>";
        $el .= "</form>";
        return ($el);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
//////// linkButton($txt, $parm)
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static public function linkButton($txt, &$parm)
    {
        $defClass = 'btnLink';

        if (!isset($parm)) $parm = array();

        if (isset($parm['*method']))
        {
            $method = $parm['*method'];
        }
        else $method = 'post';

        if (isset($parm['*width']))
        {
            $width = 'width: ' . $parm['*width'] . ';';
        }
        else $width = '';

        if (isset($parm['*height']))
        {
            $height = 'height: ' . $parm['*height'] . ';';
        }
        else $height = '';

        if (isset($parm['*padding']))
        {
            $padding = 'padding: ' . $parm['*padding'] . ';';
        }
        else $padding = '';

        if (isset($parm['*margin']))
        {
            $margin = 'margin: ' . $parm['*margin'] . ';';
        }
        else $margin = '';

        if (isset($parm['*name']))
        {
            $name = 'name=\'' . $parm['*name'] . '\' ';
        }
        else $name = '';

        if (isset($parm['*buttonClass']))
        {
            $btnClass = $parm['*buttonClass'];
        }
        else $btnClass = $defClass;

        if (isset($parm['*formClass']))
        {
            $formClass = $parm['*formClass'];
        }
        else $formClass = "button";

        if (isset($parm['*marginTop']))
        {
            $marginTop = "margin-top:" . $parm['*marginTop'] . ";";
        }
        else $marginTop = null;

        if (isset($parm['*paddingTop']))
        {
            $paddingTop = "padding-top:" . $parm['*paddingTop'] . ";";
        }
        else $paddingTop = null;

        if (isset($parm['+display']))
        {
            $display = "display:" . $parm['+display'] . ";";
        }
        else $display = null;

        if (isset($parm['*style']))
        {
            $style = $parm['*style'];
        }
        else $style = '';

        if (isset($parm['+style']))
        {
            $formStyle = $parm['+style'];
        }
        else $formStyle = '';

        $style     .= "{$padding}{$margin}{$width}{$height}{$marginTop}{$paddingTop}";
        $formStyle .= "{$display}";
        if (strlen($style) > 2) $style = 'style="' . $style . '"';
        if (strlen($formStyle) > 2) $formStyle = 'style="' . $formStyle . '"';
        $el = "<form class='" . $formClass . "' method='{$method}' action='" . '#' . "' {$formStyle}>";

        foreach ($parm as $var => $val)
        {
            if (substr($var, 0, 1) == "*" || substr($var, 0, 1) == "+") continue;
            if (substr($var, -2) == "[]" && is_array($val))
            {
                foreach ($val as $v)
                {
                    $el .= "<input type='hidden' name='{$var}' value=\"{$v}\"/>";
                }
            }
            else $el .= "<input type='hidden' name='{$var}' value=\"{$val}\"/>";
        }
        $el .= "<button {$style} type='submit' {$name} class='{$btnClass}'>{$txt}</button>";
        $el .= "</form>";
        return ($el);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////
//////// linkButton($btnLabel, $parm)
////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static public function standardButton($btnLabel, $parm)
    {
        $defaults = ['*method'      => null,
                     '*width'       => null,
                     '*height'      => null,
                     '*padding'     => null,
                     '*margin'      => null,
                     '*name'        => null,
                     '*buttonClass' => null,
                     '*formClass'   => null,
                     '*marginTop'   => null,
                     '*disabled'    => null,
                     '*paddingTop'  => null,
                     '+display'     => null,
                     '*style'       => null,
                     '+style'       => null,
            ];
        $defClass = '';

        foreach($defaults as $att=>$val)
        {
            $parm[$att] = $parm[$att] ?? $defaults[$att];
        }

        if (!empty($parm['*method']))
        {
            $method = $parm['*method'];
        }
        else $method = 'post';

        if (!empty($parm['*width']))
        {
            $width = 'width: ' . $parm['*width'] . ';';
        }
        else $width = '';

        if (!empty($parm['*height']))
        {
            $height = 'height: ' . $parm['*height'] . ';';
        }
        else $height = '';

        if (!empty($parm['*padding']))
        {
            $padding = 'padding: ' . $parm['*padding'] . ';';
        }
        else $padding = '';

        if (!empty($parm['*margin']))
        {
            $margin = 'margin: ' . $parm['*margin'] . ';';
        }
        else $margin = '';

        if (!empty($parm['*name']))
        {
            $name = 'name=\'' . $parm['*name'] . '\' ';
        }
        else $name = '';

        if (!empty($parm['*buttonClass']))
        {
            $btnClass = $parm['*buttonClass'];
        }
        else $btnClass = $defClass;

        if (!empty($parm['*formClass']))
        {
            $formClass = $parm['*formClass'];
        }
        else $formClass = "button";

        if (!empty($parm['*marginTop']))
        {
            $marginTop = "margin-top:" . $parm['*marginTop'] . ";";
        }
        else $marginTop = null;

        if (!empty($parm['*disabled']))
        {
            $disabled = " DISABLED " . ";";
        }
        else $disabled = null;

        if (!empty($parm['*paddingTop']))
        {
            $paddingTop = "padding-top:" . $parm['*paddingTop'] . ";";
        }
        else $paddingTop = null;

        if (!empty($parm['+display']))
        {
            $display = "display:" . $parm['+display'] . ";";
        }
        else $display = null;

        if (!empty($parm['*style']))
        {
            $style = $parm['*style'];
        }
        else $style = '';

        if (!empty($parm['+style']))
        {
            $formStyle = $parm['+style'];
        }
        else $formStyle = '';

        $style     .= "{$padding}{$margin}{$width}{$height}{$marginTop}{$paddingTop}";
        $formStyle .= "{$display}";
        if (strlen($style) > 2) $style = 'style="' . $style . '"';
        if (strlen($formStyle) > 2) $formStyle = 'style="' . $formStyle . '"';
        $el = "<form class='" . $formClass . "' method='{$method}' action='" . '#' . "' {$formStyle}>";

        foreach ($parm as $var => $val)
        {
            if (substr($var, 0, 1) == "*" || substr($var, 0, 1) == "+") continue;
            if (substr($var, -2) == "[]" && is_array($val))
            {
                foreach ($val as $v)
                {
                    $el .= "<input type='hidden' name='{$var}' value=\"{$v}\"/>";
                }
            }
            else $el .= "<input type='hidden' name='{$var}' value=\"{$val}\"/>";
        }
        $el .= "<button {$style} {$disabled} type='submit' {$name} class='{$btnClass}'>{$btnLabel}</button>";
        $el .= "</form>";
        return ($el);
    }
}