<?php

namespace App\Library\common\Utilities;

use Illuminate\Support\Facades\Session;

/*******************************************************************************
 * Class UserList
 *
 * Author: Marc Zev
 * Development Date: May 30, 2012
 *
 * Provides a set of static methods that are very useful
 *
 *******************************************************************************/
class UserList
{


//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    list
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
    static public function ls($dir = null, $isCommonList = true)
    {
        $lists = array();

        $dirUserList = _Get::directory('userlists');
        if ($isCommonList)
        {
            $topDir = Session::get('cart') . ((substr($dirUserList, 0, 2) == './') ? substr
                ($dirUserList, 2) : $dirUserList);
        }
        else $topDir = $dirUserList;

        if (!empty($dir)) $dir = str_replace($topDir, "", $dir);

        $flags = GLOB_MARK;

        $files = glob($topDir . $dir . "*.*", $flags);

        if (count($files) == 0) _Debug::quick($topDir . $dir . "*.*");


        foreach ($files as $file)
        {
            $idx         = pathinfo($file, PATHINFO_FILENAME);
            $lists[$idx] = $file;
        }

        return ($lists);
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    getList
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static public function getList($listName, $dir = null, $isCommonList = true)
    {
        $dirUserList = _Get::directory('userlists');
        if ($isCommonList)
        {
            $topDir = Session::get('common') . ((substr($dirUserList, 0, 2) == './') ? substr
                ($dirUserList, 2) : $dirUserList);
        }
        else $topDir = $dirUserList;

        if (!empty($dir)) $dir = str_replace($topDir, "", $dir);

        $filename = $topDir . $dir . $listName . ".php";

        if (!is_file($filename))
        {
            _Debug::error("Storm it! The {$listName} list can't be found.");
            return (false);
        }

        $list = file($filename);
        $isDB = $isList = false;

        foreach ($list as $idx => $line)
        {
            $line = trim($line);
            if (empty($line) || substr($line, 0, 1) == ';' || substr($line, 0, 2) == '//')
            {
                unset($list[$idx]);
                continue;
            }
            if (stripos($line, "++TypeDefine++") !== false)
            {
                list(, $type) = explode("=", $line);
                if (trim(strtolower($type)) == 'list') $isList = true;
                if (trim(strtolower($type)) == 'table') $isDB = true;
                unset($list[$idx]);
                continue;
            }
            $list[$idx] = $line;
        }

        $rv = array();

// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
// ... fill $rv with ini style list from a file on disk
        if ($isList)
        {
            foreach ($list as $idx => $line)
            {
                if (stripos($line, "=") === false) continue;
                list($key, $val) = explode('=', $line);
                $val = stripslashes(trim($val));
                $key = stripslashes(trim($key));
                if ((substr($val, 0, 1) == '"' && substr($val, -1) == '"') || (substr($val, 0, 1) == "'" && substr($val, -1) == "'")) $val = substr(substr($val, 0, -1), 1);
                if ((substr($key, 0, 1) == '"' && substr($key, -1) == '"') || (substr($key, 0, 1) == "'" && substr($key, -1) == "'")) $key = substr(substr($key, 0, -1), 1);
                $rv[trim($key)] = $val;
            }
        }
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
// ... fill $rv with results of a query
        if ($isDB)
        {
            foreach ($list as $idx => $line)
            {
                if (stripos($line, "=") === false) continue;
                list($key, $val) = explode('=', $line, 2);
                $val = stripslashes(trim($val));
                $key = stripslashes(trim($key));
                if ((substr($val, 0, 1) == '"' && substr($val, -1) == '"') || (substr($val, 0, 1) == "'" && substr($val, -1) == "'")) $val = substr(substr($val, 0, -1), 1);
                if ((substr($key, 0, 1) == '"' && substr($key, -1) == '"') || (substr($key, 0, 1) == "'" && substr($key, -1) == "'")) $key = substr(substr($key, 0, -1), 1);
                $instructions[trim($key)] = $val;
            }
            $sql    = self::buildSQLString($instructions);

            $result = DB::select($sql);
            if ($result->count() > 0)
            {
                foreach($result as $row)
                {
                    $rv[trim($row->name('Value'))] = trim($row->name('Display'));
                }
            }
            else $rv = array();

        }
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        return ($rv);
    }


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    buildSQLString
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static public function buildSQLString($inputVars)
    {
        $rv      = null;
        $varList = array('tableName' => 'required',
                         'database'  => false,
                         'schema'    => false,
                         'value'     => 'required',
                         'display'   => 'required',
                         'orderBy'   => false,
                         'where'     => false,
        );

        foreach ($varList as $var => $req)
        {
            if ($req && !isset($inputVars[$var])) return (false);
            if (isset($inputVars[$var])) $$var = $inputVars[$var];
        }

        $rv .= "SELECT {$value} as Value, {$display} as Display ";
        $rv .= "FROM " . ((isset($database)) ? "{$database}." : null) . ((isset($schema)) ? "{$schema}." : null) . "{$tableName}";

// ... from and joins are not yet implemented 	- to implement them the terms have to be added to the varList array
        if (isset($from))
        {
            if (stripos($from, 'from ') === false) $from = " FROM " . $from . " ";
            $rv .= $from;
        }

        if (isset($joins))
        {
            $rv .= " " . $joins;
        }
// ---------------------------------------------------------------------------
        if (isset($where))
        {
            if (stripos($where, 'where ') === false) $where = " WHERE " . $where . " ";
            $rv .= $where;
        }

        if (isset($orderBy))
        {
            if (stripos($orderBy, 'order by ') === false) $orderBy = " ORDER BY " . $orderBy . " ";
            $rv .= $orderBy;
        }

        if (empty($rv)) $rv = false;
        return ($rv);
    }
}


;
