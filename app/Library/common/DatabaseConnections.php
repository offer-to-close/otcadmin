<?php

namespace App\Library\common;

use App\Library\common\Utilities\Toumai;
use Illuminate\Database\Eloquent\Model;

class DataConnections extends Toumai
{
    public $connections = [];

    public function __construct()
    {
        parent::__construct();
        $this->connections = config('otcCommon.otcSystems.connections');
    }

    public function getConnection($key=null)
    {
        if (is_null($key)) return $this->connections;
        if (isset($this->connections[$key])) return [];
        return $this->connections[$key];
    }

    public function setConnection(Model $model, $key='default') :Model
    {
        if (is_null($key)) return $model;
        if (isset($this->connections[$key])) return $model;
        return $model->setConnection($key);
    }

}