<?php

namespace App\Library\payments;

use App\Library\common\Utilities\_Arrays;
use App\Library\common\Utilities\_Log;
use App\Library\common\Utilities\Toumai;
use Exception;
use Illuminate\Support\Facades\Log;
use Cartalyst\Stripe\Laravel\Facades\Stripe;

class StripeService extends Toumai
{
    private $publicKey, $secretKey, $service;
    private $requestResponse;
    private $commands = [];
    private $curlParameters = ['verbose' => false,];
    const  STRIPE_CC_VALID = 'valid';
    const  STRIPE_CC_BAD_NUMBER = 'badNumber';
    const  STRIPE_CC_VALID_NUMBER_WILL_NOT_CHARGE = 'validNumberSillNotCharge';
    const  STRIPE_CC_SUCCEEDS_WITH_RISK = 'succeedsWithRisk';
    const  STRIPE_CC_DECLINED = 'declined';
    const  STRIPE_CC_DECLINED_FRAUD = 'declinedFraud';
    const  STRIPE_CC_BAD_CVC = 'badCvc';
    const  STRIPE_CC_EXPIRED = 'expired';
    const  STRIPE_CC_ERROR = 'error';

    public function __construct()
    {
        $this->initialize();
    }

    //    public function paymentIntent()
    //    {
    //        $headers = ['Content-Type: application/json'];
    ////        $action = strtolower($action);
    //        $url = config('otcServices.dbServices.otc')['apiUrl'];
    //
    //        $curl = curl_init();
    //
    //        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    //        curl_setopt($curl, CURLOPT_URL, $url );
    //        if (($this->curlParameters['verbose'] ?? false)) curl_setopt($curl, CURLOPT_VERBOSE,
    //            // 1);
    //        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    //        curl_setopt($curl, CURLOPT_POST, 1);
    //        //        curl_setopt($curl, CURLOPT_STDERR, $fh);
    //        //        curl_setopt($curl, CURLOPT_USERPWD, 'API:&&otcAccess%');
    //
    //        $rv = curl_exec($curl);
    //
    //    }
    public function retrieveCustomers($customerID = null)
    {
        $customers = $this->service->customers();
        if (is_null($customerID)) return $customers->all();
        else  return $customers->find($customerID);
    }

    public function createServiceProduct($productName, $productType = 'service')
    {
        $product = \Stripe\Product::create([
            'name' => $productName,
            'type' => $productType,
        ]);
    }

    public function createCheckoutSession($parameters = []): \Stripe\Checkout\Session
    {
        $defaults = ['paymentMethod' => 'card',
                     'name'          => 'Product_Name',
                     'description'   => null,
                     'images'        => [],
                     'amount'        => 1,        // dollars
                     'currency'      => 'usd',
                     'quantity'      => 1,
                     'success_url'   => route('stripe.checkout.success'),
                     'cancel_url'    => route('stripe.checkout.cancel'),
        ];

        $parameters = _Arrays::assignDefaults($parameters, $defaults);

        $sessionParameters = [
            'payment_method_types' => $parameters['paymentMethod'],
            'line_items'           => [[
                                           'name'        => 'Intro-Offer',                               // todo
                                           'description' => 'One Offer To Close Transaction',            // todo
                                           'images'      => [resource_path('/prelogin/logo.png')], //todo
                                           'amount'      => $parameters['amount'] ?? 0,
                                           'currency'    => $parameters['currency'] ?? null,
                                           'quantity'    => $parameters['quantity'] ?? 0,
                                       ]],
            'success_url'          => $parameters['success_url'],
            'cancel_url'           => $parameters['cancel_url'],
        ];

        //    try
        //    {
        $session = \Stripe\Checkout\Session::create($sessionParameters);
        //    }
        //    catch (Exception $e)
        //    {
        //        $context = ['&&-EXCEPTION'=>$e->getMessage(),
        //                    'Error Location'=>$e->getFile() . ':: ' .$e->getLine(),
        //                    'Trace'=>_Log::stackDump(true),
        //                    'parameters'=>$parameters,
        //                    'sessionParameters'=>$sessionParameters ,
        //                    __METHOD__=>__LINE__];
        //        Log::error($context);
        //        if (isServerLocal()) dd($context);
        //    }
        return $session;
    }

    public function submitCommand($command, $parameters)
    {
        $validCommands = ['charge',];

        switch ($command)
        {
            case 'charge':
                break;
            default:
                return false;
        }

        try
        {
            // Use Stripe's library to make requests...
        }
        catch (\Stripe\Exception\CardException $e)
        {
            // Since it's a decline, \Stripe\Exception\CardException will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];

            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $e->getStripeError()->type . "\n");
            print('Code is:' . $e->getStripeError()->code . "\n");
            // param is '' in this case
            print('Param is:' . $e->getStripeError()->param . "\n");
            print('Message is:' . $e->getStripeError()->message . "\n");
        }
        catch (\Stripe\Exception\RateLimitException $e)
        {
            // Too many requests made to the API too quickly
        }
        catch (\Stripe\Exception\InvalidRequestException $e)
        {
            // Invalid parameters were supplied to Stripe's API
        }
        catch (\Stripe\Exception\AuthenticationException $e)
        {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
        }
        catch (\Stripe\Exception\ApiConnectionException $e)
        {
            // Network communication with Stripe failed
        }
        catch (\Stripe\Exception\ApiErrorException $e)
        {
            // Display a very generic error to the user, and maybe send
            // yourself an email
        }
        catch (Exception $e)
        {
            // Something else happened, completely unrelated to Stripe
        }
    }

    private function initialize()
    {
        $server          = (isServerLive()) ? '.live' : '.test';
        $this->publicKey = config('payment.stripe.publishableKey' . $server);
        $this->secretKey = config('payment.stripe.secretKey' . $server);
        //       Stripe::setApiKey($this->secretKey);
        $this->service = Stripe::make($this->secretKey);
    }

    public static function _test()
    {
        $charge = \Stripe\Charge::create([
            'amount'        => 999,   // in cents
            'currency'      => 'usd',
            'source'        => 'tok_visa',
            'receipt_email' => 'mzev@OfferToClose.com',
            'metadata'      => ['transactionID' => 34,],
        ]);

        $id     = $charge->id;
        $status = $charge->status;
        dd(['id' => $id, 'status' => $status, 'charge obj' => $charge]);
    }

    public function createCustomer($data = [])
    {
        $defaults = [
            //'description' => 'Arbitrary text description',
            'source'   => $this->accessToken,
            'address'  => [
                'line1'       => null,
                'line2'       => null,
                'city'        => null,
                'state'       => null,
                'postal_code' => null,
                'country'     => null,
            ],
            'email'    => null,
            'metadata' => [
                'otcService'     => 'otc',
                'transactionID'  => null,
                'payerUserID'    => null,
                'discountAmount' => null,
                'discountName'   => null,
                'discountID'     => null,
            ],
            'name'     => null,
            'phone'    => null,
        ];

        foreach ($defaults as $key => $def)
        {
            if (is_array($def))
            {
                foreach ($def as $k => $d)
                {
                    $data[$key][$k] = $data[$key][$k] ?? $d;
                }
            }
            else
            {
                $data[$key] = $data[$key] ?? $def;
            }
        }
        $customer = \Stripe\Customer::create($data);
        return $customer;
    }

    public function createCharge($data = [])
    {
        $discount_id = null;

        $defaults = [
            'amount'      => 0,  // in cents
            'currency'    => 'usd',
            'description' => null,
            'customer'    => null, // otcUsers.ID
        ];

        foreach ($defaults as $key => $def)
        {
            $data[$key] = $data[$key] ?? $def;
        }

        $idempotency_key = request()->post('idempotency_key');

        try
        {
            $charge = \Stripe\Charge::create($data, [
                'idempotency_key' => $idempotency_key,
            ]);

            $charge_succeeded = true;
        }
        catch (\Stripe\Error\Card $e)
        {
            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $error_message = '<p>Payment declined: ' . $err['message'] . ' Please try another form of payment.</p>';
        }
        catch (\Stripe\Error\RateLimit $e)
        {
            // Too many requests made to the API too quickly
            $error_message = '<p>Too many requests made too quickly. Please wait and try again.</p>';
        }
        catch (\Stripe\Error\InvalidRequest $e)
        {
            // Invalid parameters were supplied to Stripe's API (e.g. reusing a token by refreshing/resubmitting the payment page).
            $body          = $e->getJsonBody();
            $err           = $body['error'];
            $error_message = '<p>Invalid request: ' . $err['message'] . ' Please contact us for assistance.</p>';
        }
        catch (\Stripe\Error\Authentication $e)
        {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $error_message = '<p>Authentication failed. Please contact us for assistance.</p>';
        }
        catch (\Stripe\Error\ApiConnection $e)
        {
            // Network communication with Stripe failed
            $error_message = '<p>Network communication failed. Please wait and try again.</p>';
        }
        catch (\Stripe\Error\Base $e)
        {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $body = $e->getJsonBody();
            $err  = $body['error'];

            if (stristr($err['message'], 'idempotent'))
            {
                $error_message = '<p>Our records indicate that we may already have successfully charged your card. Please <a href="/receipt/">review your receipt</a>, or <a href="/contact/">contact us</a> for assistance.</p>';
            }
            else
            {
                $error_message = '<p>Payment processing error occurred: ' . $err['message'] . ' Please wait and try again, or <a href="/contact/">contact us</a> for assistance.</p>';
            }
        }
        catch (Exception $e)
        {
            // Something else happened, completely unrelated to Stripe
            $error_message = '<p>Sorry, an error occurred. Please try again, or contact us for assistance.</p>';
        }

        if ($charge_succeeded)
        {

            // Save the payment if the charge succeeded.
            $auth_id = $charge->id;

            if (!$discount_id)
            {
                $discount_id = 'null';
            }

        }
    }

    /**
     * Returns either a specific card number that has a special function when used with Stripe, or the list of all the special card numbers with
     * descriptions of their purpose.
     *
     * @param null $type
     *
     * @return array|mixed|null With a defined $type as argument a card number, with $type = * the full array of card numbers are returned the
     * default value is for the card number that returns and valid.
     */
    public function getTestCard($type = null)
    {
        $cards = [
            $this->STRIPE_CC_VALID                        => ['number' => '4242 4242 4242 4242', 'description' => 'No errors'],
            $this->STRIPE_CC_BAND_NUMBER                  => ['number' => '4242 4242 4242 4241', 'description' => 'Charge declined with incorrect_number code'],
            $this->STRIPE_CC_VALID_NUMBER_WILL_NOT_CHARGE => ['number' => '4000 0000 0000 0341', 'description' => 'Attaching this care to a Customer [API] object succeeds, but attempts to charge the customer fail'],
            $this->STRIPE_CC_SUCCEEDS_WITH_RISK           => ['number' => '4000 0000 0000 9235', 'description' => 'Charge succeeds with a risk_level of "elevated" and placed into review'],
            $this->STRIPE_CC_DECLINED                     => ['number' => '4000 0000 0000 0002', 'description' => 'Charge is declined with a card_declined code'],
            $this->STRIPE_CC_DECLINED_FRAUD               => ['number' => '4000 0000 0000 0019', 'description' => 'Charge is declined with a card_declined code and a fraudulent reason'],
            $this->STRIPE_CC_BAD_CVC                      => ['number' => '4000 0000 0000 0127', 'description' => 'Charge declined with an incorrect_cvc code'],
            $this->STRIPE_CC_EXPIRED                      => ['number' => '4000 0000 0000 0069', 'description' => 'Charge is declined with an expired_card code'],
            $this->STRIPE_CC_ERROR                        => ['number' => '4000 0000 0000 0119', 'description' => 'Charge declined with a processing_error code'],
        ];

        $type = $type ?? $this->STRIPE_CC_VALID;

        if ($type == '*') return $cards;
        if (!isset($cards[$type])) return null;
        return $cards[$type]['number'];
    }
}
