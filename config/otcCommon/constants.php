<?php

return [

    'UI' => [
        'version'  => env('UI_VERSION', '1a'),
        'fallback' => env('UI_VERSION_FALLBACK', '1a'),
    ],

    'Prelogin' => [
        'version'  => env('PRELOGIN_VERSION', '1a'),
        'fallback' => env('PRELOGIN_VERSION_FALLBACK', '1a'),
    ],


    'API' => [
        'version'  => env('API_VERSION', '1a'),
    ],

    'API_PW' => [
        'otc' => env('OTC_API_PW', 'API:KnockThreeTimes'),
        'oh' => env('OH_API_PW'),
        'offers' => env('OFFERS_API_PW'),
    ],


    /*
    |--------------------------------------------------------------------------
    | User Types
    |--------------------------------------------------------------------------
    |
    | These values are used by lookup tables to identify different levels of access people
    | have within the system. There are two types of values set; String values
    | and numeric values, the higher the value, the higher access. The way to measure access by
    | comparing the user's type value to the level of access associated with the page/process.
    |
    */

    'UserTypes' => [
        'z' => ['display' => 'super', 'value' => 1001, 'isVisible'=>false],
        'd' => ['display' => 'dev',   'value' => 400, 'isVisible'=>true],
        'a' => ['display' => 'admin', 'value' => 300, 'isVisible'=>true],
        's' => ['display' => 'staff', 'value' => 200, 'isVisible'=>true],
        'u' => ['display' => 'user',  'value' => 100, 'isVisible'=>true],
        'g' => ['display' => 'guest', 'value' => 1, 'isVisible'=>true],
    ],


    /*
    |--------------------------------------------------------------------------
    | Access Parameters
    |--------------------------------------------------------------------------
    |
    | This is the list of parameters that can be used to qualify a user in
    | the OtcUsers table
    |
    */

    'AccessParameters' => [
        'type' => ['display' => 'User Login Type', 'value' => 'type', 'notes'=>'found in the lu_UserTypes table', 'isVisible'=>true],
    ],


    /*
    |--------------------------------------------------------------------------
    | OTC Services
    |--------------------------------------------------------------------------
    |
    | This is the list of websites that are part of the Offer To Close suite
    |  of services
    |
    */

    'OtcServices' => [
        'otc' => ['code'=>'otc', 'display' => 'Offer To Close', 'UrlPublic' => 'https://www.OfferToClose.com', 'UrlApi' => 'https://api.OfferToClose.com', 'isVisible'=>true],
        'oh' => ['code'=>'oh', 'display' => 'OTC Open House', 'UrlPublic' => 'https://www.otcOpenHouse.com', 'UrlApi' => 'https://api.otcOpenhouse.com', 'isVisible'=>true],
    ],


];