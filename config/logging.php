<?php

return [

    'channels' => [
        'stack' => [
            'driver' => 'stack',
            'channels' => ['daily'],
        ],

        'syslog' => [
            'path' => storage_path('logs/sys.log'),
            'driver' => 'syslog',
            'level' => 'debug',
        ],


        'daily' => [
            'driver' => 'daily',
            'emoji' => ':boom:',
            'level' => 'debug',
//            'path' => storage_path('logs/sys.log'),
        ],

        'stack' => [
            'driver' => 'stack',
            'name' => 'channel-name',
            'channels' => ['daily', 'slack'],
        ],

        'mail' => [
            'driver' => 'daily',
            'tap' => [App\Logging\CustomizeFormatter::class],
            'path' => storage_path('logs/mail.log'),
            'level' => 'critical',
        ],
    ],
];