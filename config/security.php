<?php

return [


    /*
    |--------------------------------------------------------------------------
    | Handshake
    |--------------------------------------------------------------------------
    |
    | This is the secure password needed by other services to communicate with otcAdmin
    |
    */

    'secureAdminHandshake' => env('adminHandshake', 'test'),
];