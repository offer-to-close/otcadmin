@extends('1a.layouts.payment')
<?php
$data = isset($data) ? $data : get_defined_vars()['__data'];
$checkoutSession =  \Stripe\Checkout\Session::retrieve($stripeSession->id ?? 0);
?>
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Stripe Payments</div>


                    <section class="container">
                        <div>
                            <h1>Single photo</h1>
                            <h4>Purchase a Pasha original photo</h4>
                            <div class="pasha-image">
                                <img
                                        src="https://picsum.photos/280/320?random=4"
                                        width="140"
                                        height="160"
                                />
                            </div>
                        </div>
                        <div class="quantity-setter">
                            <button class="increment-btn" id="subtract" disabled>
                                -
                            </button>
                            <input type="number" id="quantity-input" min="1" value="1" />
                            <button class="increment-btn" id="add">+</button>
                        </div>
                        <p class="sr-legal-text">Number of copies (max 10)</p>

                        <button id="submit">
                            Buy for $<span id="total">5</span>.00
                        </button>
                    </section>


                    <div class="panel-body">

                        <form action="charge.php" method="post">
                            <script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                    data-key="{{config('payment.stripe.publishableKey' . (isServerLive() ? '.live' : '.test'))}}"
                                    data-description="Access for a year"
                                    data-amount="5000"
                                    data-locale="auto"></script>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

@endsection