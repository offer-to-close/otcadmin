@extends('1a.layouts.otc')
<?php
$data = isset($data) ? $data : get_defined_vars()['__data'];

$actions = [
                [
                    'label'=>'Review Member Request',
                    'description'=>'Decide if people who want to be members can be.',
                    'url'=>route('otc.reviewMemberRequest'),
                ],
                [
                    'label'=>'Something Else',
                    'description'=>'Yada yada',
                    'url'=>route('otc.reviewMemberRequest'), // change
                ],
    ];

?>
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Offer To Close Dashboard</div>

                    <div class="panel-body">

                        <table>
                            <tr><th>Action</th><th style="width:10px"></th><th>Description</th></tr>
                            @foreach($actions as $action)
                                <tr><td><a href="{{$action['url']}}">{{$action['label']}}</a></td><td></td><td>{{$action['description']}}</td></tr>
                            @endforeach
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

