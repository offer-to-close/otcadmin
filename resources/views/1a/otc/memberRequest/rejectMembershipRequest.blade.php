@if(Session::has('data_saved'))
    @include('2a.partials._successMessage', [
        '_Message' => Session::get('data_saved'),
    ])
@endif
@extends('1a.otc.layouts.master')
@section('content')
    <?php

    /* ... Expected variables ...
            $transactionID
            $users :: The list of users that can be invited by TC
            $senderRole :: Is the sender TC the 'btc' or the 'stc'
    */
    $action = $action ?? route('otc.request.reject');

    $requestColumns = ['PropertyAddress'=>'Address', 'Email'=>'Email', 'Company'=>'Company', 'ReasonRequested'=>'Reason'];

    $displayFields = [];
    foreach ($requestColumns as $fld=>$header)
    {
        if (isset($memberRequest->{$fld}) && !empty($memberRequest->{$fld})) $displayFields[$header] = $memberRequest->{$fld};
    }

    $transactionFields = [];
    if ($transaction)
    {
        $tmp = ['Buyer\'s Agent'=>'buyersAgent', 'Buyer\'s TC'=>'buyersTC', 'Seller\'s Agent'=>'sellersAgent', 'Seller\'s TC'=>'sellersTC'];

        $t['Property Address'] = \App\Library\otc\AddressVerification::formatAddress($transaction['property'], 1);

        foreach ($tmp as $key=>$table)
        {
            $rec = is_null($transaction[$table]->first()) ? [] : $transaction[$table]->first()->toArray();
            if (!isset($rec['NameFirst'])) $rec['NameFirst'] = '';
            if (!isset($rec['NameLast'])) $rec['NameLast'] = '';
            $t[$key] = (isset($rec['NameFirst']) ? $rec['NameFirst'] : null) . ' ' .
                       isset($rec['NameLast']) ? $rec['NameLast'] : null;
        }
    }

    if (empty(trim($displayRole))) $displayRole = 'member';

    ?>
    @if (\Session::has('failure'))
        <div class="alert alert-danger">
            <h3 class="alert-danger-h3">{!! \Session::get('failure') !!}</h3>
        </div>
    @endif

    <div class="col-sm-offset-1 col-sm-10 offer_page clearfix">
        <div class="col-sm-6">

            <h2 class="offer_page_gen_title otc-red"><i class="far fa-hand-paper"></i> Reject Member Request <i class="far fa-thumbs-down"></i></h2> </div>
        <form method="post" action="{{$action}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="memberRequestID" value="{{$memberRequestID ?? 0}}" />
            <input type="hidden" name="address" value="{{$address ?? null}}" />

            <div class="col-sm-12 offer_page_main clearfix">
                <div class="col-lg-offset-3 col-lg-6  col-md-offset-2 col-md-8 col-sm-offset-1 col-sm-10">

                    <h3>{{$memberRequest->NameFirst ?? '"'}} {{$memberRequest->NameLast ?? '"'}} has requested to become a<br/> {{$displayRole ?? 'member'}} </h3>
                    <br/>
                    <p><label style="width:275px;"  for="DecisionReason">Reason for rejection:</label>
                        <br/>&nbsp;
                        <textarea style="width:475px; height:150px;"  name="DecisionReason"></textarea></p>

                @foreach($displayFields ?? [] as $label=>$value)
                        <p><label style="width:75px;"  for="{{$label}}">{{$label}}:</label>&nbsp;
                            <input style="width:400px;"  type="text" name="{{$label}}" value="{{$value}}" READONLY></p>
                    @endforeach

                    @if($transaction)
                        <h4>Transaction Details</h4>
                        @foreach($t ?? [] as $label=>$value)
                            <p><label style="width:100px;" for="{{$label}}">{{$label}}</label>:&nbsp;
                                <input style="width:100px;" type="text" name="{{$label}}" value="{{$value}}"></p>
                        @endforeach
                    @endif

                    <strong class="red">Reject Request?&nbsp;
                        <button name="inviteButton" value="rejectInvitation" type="submit">Yes</button>
                        <button name="inviteButton" value="cancelInvitation" type="submit">No</button>
                    </strong>

                    <div style="clear: both"></div>
                </div>
            </div>
        </form>
    </div>
@endsection

