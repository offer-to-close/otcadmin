@extends('1a.otc.layouts.master')
<?php
$viewNames = explode('.', $_viewName);
$title = 'Approve or Reject Membership Requests';
$showData = true;

//    $action = $action ?? route('invite.confirm');

?>
@section('custom_css')
    @include ('1a.css.basicTable')
@endsection
@section('content')
    <div id="app">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="add-new-transaction-title col-xs-10">
                    <div class="col-xs-12">
                        <h1 style="color: {{config('otc.color.teal')}}">Offer To Close: Staff Menu</h1>

                        <h2>{{$title}}</h2>
                    </div>
                </div>

                <!-- ***************************************************************************************************** -->
                @if ($showData)
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="add-new-transaction-title col-xs-10">
                                <div class="col-xs-12">

                                </div>
                                <div class="col-xs-12">
                                    <hr/>
                                    {!! $statusTable !!}
                                </div>
                            </div>
                        </div>
                    </div>
            @endif
            <!-- ***************************************************************************************************** -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <p>
                        <strong>Total Transactions: {{$totalCount}}</strong>
                    </p>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12">
                    <a href="{{route('otc.main')}}"><button type="submit">Back to Menu</button></a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $('.mark_spam').click(function () {
                let self = $(this);
                let recordID = self.data('id');
                let markSpamRoute = '{{route('otc.markRequestSpam', ['ID' => ':id'])}}';
                markSpamRoute = markSpamRoute.replace(':id', recordID);
                Swal2({
                    type: 'question',
                    text: 'Mark as spam?',
                    confirmButtonText: 'Yes',
                    showCancelButton: true,
                    preConfirm: ()=> {
                        $.ajax({
                            url: markSpamRoute,
                            method: 'GET',
                            success: function (r) {
                                if(r.status == 'success')
                                {
                                    Swal2({
                                        type: 'success',
                                        text: r.message,
                                        timer: 1000,
                                        showConfirmButton: false,
                                        allowOutsideClick: false,
                                    });
                                    self.parents('tr').addClass('deactivated-noevents');
                                }
                                else if (r.status == 'fail')
                                {
                                    genericError(r.message);
                                }
                                else
                                {
                                    genericError();
                                }
                            },
                            error: function (err) {
                                genericError(err);
                            },
                        });
                    },
                });
            });
        });
    </script>
@endsection