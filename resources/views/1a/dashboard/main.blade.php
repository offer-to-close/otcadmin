@extends('1a.layouts.main')
<?php
$data = isset($data) ? $data : get_defined_vars()['__data'];
?>
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">otcAdmin Summary</div>

                    <div class="panel-body">

                        <table>
                            <thead>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th> Integrated Services</th>
                                <th></th>
                                <th> URL</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data['services'] as $code=>$service)
<?php
    $checked = ($data['currentService'] == $code) ? 'CHECKED' : '';
    $value = $code;
//    dump('code = ' . $code . ' == current = ' . $data['currentService']);
?>
                                <tr>
                                    <td> <a href="">Switch to&nbsp;&nbsp;&nbsp;&nbsp;</a></td>
                                    <td><input type="radio" name="_service" value="{{$value}}" READONLY {{$checked}}></td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td> {{$service['display']}} </td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td> {{$service['UrlPublic']}} </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

