<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Settings', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('Code');
            $table->string('Display');
            $table->string('Description');
            $table->string('OtcServices_Code');
            $table->string('Type')->comment('Type of setting this is for, example: Email, SMS');
            $table->string('Category')->comment('For example: Marketing Emails');
            $table->string('ValueType')->comment('The type of data the OtcUserPreference needs to be, example: boolean');
            $table->string('DefaultValue')->comment('Value when creating a new OtcUserPreferences record for this setting.');
            $table->string('Notes')->nullable();
            $table->boolean('isActive')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Settings');
    }
}
