<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PayServices', function (Blueprint $table) {
            $table->increments('ID');

            $table->string('Name', 25)->unique();
            $table->string('BaseUri', 250)->comment('Base URI to access service API');
            $table->string('Class', 200)->comment('Class with which to connect to Service');
            $table->string('TestKey')->nullable()->comment('API key used for testing');
            $table->string('LiveKey')->nullable()->comment('API key used for production');

            $table->string('Notes')->nullable();
            $table->boolean('isTest')->nullable()->comment = 'Standard table field used for testing';

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PayServices');
    }
}
