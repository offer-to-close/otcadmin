<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePasswordResetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo __CLASS__ ;

        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email', 155)->index();
            $table->string('token');
            $table->timestamp('created_at')->nullable();
        });
        echo ' ... complete.' . PHP_EOL;

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('password_resets');
    }
}
