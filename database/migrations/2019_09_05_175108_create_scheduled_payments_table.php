<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduledPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ScheduledPayments', function (Blueprint $table) {
            $table->increments('ID');

            $table->integer('PaymentAccounts_ID')->comment('Foreign key to PaymentAccounts.ID');
            $table->date('DateScheduled');
            $table->dateTime('TimeAttempted');
            $table->string('AttemptResult')->comment('Return from pay service');
            $table->float('AmountDue')->comment('The amount of the attempted charge');
            $table->float('AmountPaid')->comment('The amount actually charged');

            $table->string('Notes')->nullable();
            $table->boolean('isTest')->nullable()->comment = 'Standard table field used for testing';

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ScheduledPayments');
    }
}
