<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatelogPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_Payments', function (Blueprint $table) {
            $table->increments('ID');

            $table->integer('PaymentAccounts_ID')->comment('Foreign key to PaymentAccounts.ID');
            $table->date('DateAttempted');
            $table->float('AmountAttempted')->comment('How much (in USD) is the charge');
            $table->boolean('isChargeSuccess')->comment('How often does the user get charged. Foreign key to lookup table lu_PaymentFrequencies.Value');
            $table->string('ServiceReply')->comment('How many uses does user get per charge period. Zero equals infinite. Negative means that the |CountPer| - <number used> is rolled over to next charge period');
            $table->float('AmountCharged')->default(0)->comment('How much was actually charged');

            $table->string('Notes')->nullable();
            $table->boolean('isTest')->nullable()->comment = 'Standard table field used for testing';

            $table->timestamps();
            $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_Payments');
    }
}
