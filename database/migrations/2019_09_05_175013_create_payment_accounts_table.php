<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PaymentAccounts', function (Blueprint $table) {
            $table->increments('ID');

            $table->string('OtcServices_Code', 25)->comment('Foreign key to OtcServices.Code');
            $table->integer('OtcUsers_ID')->comment('Foreign key to OtcUsers.ID');
            $table->string('PayServices_Name', 200)->comment('Foreign key to PayServices_Name');
            $table->string('AccountID')->default('')->comment('Account # associated with this user and this pay service');
            $table->string('lu_PaymentFrequencies_Value')->comment('How often does the user get charged. Foreign key to lookup table lu_PaymentFrequencies.Value');
            $table->integer('CountPer')->default(0)->comment('How many uses does user get per charge period. Zero equals infinite. Negative means that the |CountPer| - <number used> is rolled over to next charge period');
            $table->float('Amount')->comment('How much (in USD) is charged per attempt');
            $table->date('NextBillDate')->comment('The date the next attempt will be made to charge amount.');

            $table->string('Notes')->nullable();
            $table->boolean('isTest')->nullable()->comment = 'Standard table field used for testing';

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PaymentAccounts');
    }
}
