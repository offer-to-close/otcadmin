<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLkAggregatedusersOtcServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo __CLASS__ ;

        if (!Schema::hasTable('OtcUserAccess'))
        {

            Schema::create('OtcUserAccess', function (Blueprint $table)
            {
                $table->increments('ID');

                $table->integer('OtcUsers_ID');
                $table->integer('OtcServices_ID');
                $table->string('Parameter')->default('')->comment('Identifies the type of parameter value being set');
                $table->string('Value')->default('')->comment('The parameter\'s value');

                $table->boolean('isActive')->nullable()->default(true);

                \App\Library\common\Utilities\MigrationHelpers::assignStandardSwahFields($table);
            });
        }
        echo ' ... complete.' . PHP_EOL;

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lk_UsersOtcServices');
    }
}
