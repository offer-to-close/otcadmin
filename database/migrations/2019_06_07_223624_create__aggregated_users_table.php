<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAggregatedUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo __CLASS__ ;
        $sql = <<<END
CREATE TABLE `OtcUsers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NameFirst` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameLast` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '/images/avatar.jpg',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Notes` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Status` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateFirstLogin` timestamp NULL DEFAULT NULL,
  `DateLastLogin` timestamp NULL DEFAULT NULL,
  `DateClosed` timestamp NULL DEFAULT NULL,
  `LoginInvitations_ID` int(11) DEFAULT NULL COMMENT 'Foreign Key to LoginInvitations table',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ActivationCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ActivationStatus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isTest` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
END;

        if (!Schema::hasTable('OtcUsers')) \Illuminate\Support\Facades\DB::statement($sql);

        echo ' ... complete.' . PHP_EOL;

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('AggregatedUsers');
    }
}
