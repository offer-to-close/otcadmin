<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtcServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo __CLASS__;

        Schema::create('OtcServices', function (Blueprint $table)
        {
            $table->increments('ID');

            $table->string('Code', 25)->unique()->comment('The value that gets stored');
            $table->string('Display', 50)->comment('What gets displayed for this value');
            $table->string('Notes')->nullable();
            $table->string('UrlPublic')->nullable();
            $table->string('UrlApi')->nullable();
            $table->float('Order')->default(1)->comment('optional field that can be used to create an \'arbitrary\' sort order.');
            $table->boolean('isActive')->nullable()->default(true);
            $table->boolean('isTest')->nullable()->default(0)->comment('Standard table field used for testing');

            $table->softDeletes();
        });
        echo ' ... complete.' . PHP_EOL;

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('OtcServices');
    }
}
