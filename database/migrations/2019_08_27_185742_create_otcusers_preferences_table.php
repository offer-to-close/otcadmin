<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtcusersPreferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('OtcUserPreferences', function (Blueprint $table) {
            $table->increments('ID');
            $table->bigInteger('OtcUsers_ID');
            $table->string('Settings_Code');
            $table->string('RoleCode')->nullable()->comment('The role code this preference is for, a user may have multiple roles.');
            $table->string('Value')->comment('The value of this setting for this user and role');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('OtcUserPreferences');
    }
}
