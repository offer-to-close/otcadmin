<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('OtcUsers', function (Blueprint $table) {
            $table->string('SecondaryPhone')->after('NameLast')->nullable();
            $table->string('PrimaryPhone')->after('NameLast')->nullable();
            $table->string('Zip')->after('NameLast')->nullable()->default('');
            $table->string('State')->after('NameLast')->nullable()->default('');
            $table->string('City')->after('NameLast')->nullable()->default('');
            $table->string('Unit')->after('NameLast')->nullable()->default('');
            $table->string('Street2')->after('NameLast')->nullable()->default('');
            $table->string('Street1')->after('NameLast')->nullable()->default('');
            $table->string('License')->after('NameLast')->nullable()->comment('License in the otcUsers_State');
            $table->string('Company')->after('NameLast')->nullable()->comment('The company the user belongs to.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('OtcUsers', function (Blueprint $table) {
            $table->dropColumn('SecondaryPhone');
            $table->dropColumn('PrimaryPhone');
            $table->dropColumn('Zip');
            $table->dropColumn('State');
            $table->dropColumn('City');
            $table->dropColumn('Unit');
            $table->dropColumn('Street2');
            $table->dropColumn('Street1');
            $table->dropColumn('License');
            $table->dropColumn('Company');
        });
    }
}
