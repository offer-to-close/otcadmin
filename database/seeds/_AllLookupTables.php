<?php

use Illuminate\Database\Seeder;

class _AllLookupTables extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            lu_AccessParameters::class,
            lu_UserTypes::class,
            OtcServices::class,
            SettingsSeeder::class,
        ]);
    }
}
