<?php

use Illuminate\Database\Seeder;

class lu_UserTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        dump(__CLASS__);
        dump(config('otcCommon.constants.UserTypes'));

        $userTypes = config('otcCommon.constants.UserTypes', []);
        $values = array_keys($userTypes);
        $intValues = array_column($userTypes, 'value');
        $display = array_column($userTypes, 'display');
        $isVisible = array_column($userTypes, 'isVisible');
        $isViewable = $values;

        // ... Make is so the Super user type can be easily excluded from lists
        if (($key = array_search('z', $isViewable)) !== false) unset($isViewable[$key]);

        $tbl = 'lu_UserTypes';
        DB::table($tbl)->delete();
        $max = DB::table($tbl)->max('id') + 1;
        DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);

        foreach($values as $idx=>$value)
        {
            DB::table($tbl)->insert([
                'Value'       => $values[$idx],
                'Display'     => $display[$idx],
                'Notes'       => '',
                'Order'       => $isVisible[$idx] ? 1 : 0,
                'ValueInt'    => $intValues[$idx],
            ]);
        }

    }
}
