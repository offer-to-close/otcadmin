<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = DB::table('Settings');
        $table->truncate();

        /*** Seeds for Offer To Close Service ***/
        $app = 'otc';
        $table->insert([
            'Code'              => 'task-reminder-001',
            'Display'           => 'Task Reminders',
            'Description'       => 'Whether or not the user should receive task reminders via email.',
            'OtcServices_Code'  => $app,
            'Type'              => 'Email',
            'Category'          => 'Daily Update Emails',
            'ValueType'         => 'boolean',
            'DefaultValue'      => '1',
        ]);
    }
}
