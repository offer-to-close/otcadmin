<?php

use Illuminate\Database\Seeder;

class OtcUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tbl = 'OtcUsers';
        DB::table($tbl)->delete();
        $max = DB::table($tbl)->max('id') + 1;
        DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);

        $usr = 0;
        ++$usr;
        DB::table('users')->insert([
            'Name'     => 'Marc',
            'Email'    => 'marc' . '@tests.casa',
            'Password' => '$2y$10$I6k8wUiNqdExHguUtC1xIeiOg.xYrn9p6TPNK18uG8dgk.uruOOQu' ,
        ]);

        ++$usr;
        DB::table('users')->insert([
            'Name'     => 'Bryan',
            'Email'    => 'bryan' . '@tests.casa',
            'Password' => '$2y$10$I6k8wUiNqdExHguUtC1xIeiOg.xYrn9p6TPNK18uG8dgk.uruOOQu' ,
        ]);

    }
}
