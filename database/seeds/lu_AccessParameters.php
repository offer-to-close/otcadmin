<?php

use Illuminate\Database\Seeder;

class lu_AccessParameters extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        dump(__CLASS__);
        dump(config('otcCommon.constants.AccessParameters'));

        $parameters = config('otcCommon.constants.AccessParameters');
        $values = array_keys($parameters);
        $display = array_column($parameters, 'display');
        $isVisible = array_column($parameters, 'isVisible');
        $isViewable = $values;

        $tbl = 'lu_AccessParameters';
        DB::table($tbl)->delete();
        $max = DB::table($tbl)->max('id') + 1;
        DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);

        foreach($values as $idx=>$value)
        {
            DB::table($tbl)->insert([
                'Value'       => $values[$idx],
                'Display'     => $display[$idx],
                'Notes'       => '',
                'Order'       => $isVisible[$idx] ? 1 : 0,
            ]);
        }
    }
}
