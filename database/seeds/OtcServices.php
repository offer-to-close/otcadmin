<?php

use Illuminate\Database\Seeder;

class OtcServices extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        dump(__CLASS__);
        dump(config('otcCommon.constants.OtcServices'));

        $services = config('otcCommon.constants.OtcServices', []);
        $values = array_keys($services);
        $display = array_column($services, 'display');
        $urlPublic = array_column($services, 'UrlPublic');
        $urlApi = array_column($services, 'UrlApi');
        $isActive = array_column($services, 'isActive');
        $isVisible = array_column($services, 'isVisible');
        $isViewable = $values;

        $tbl = 'OtcServices';
        DB::table($tbl)->delete();
        $max = DB::table($tbl)->max('id') + 1;
        DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);

        foreach($values as $idx=>$value)
        {
            DB::table($tbl)->insert([
                'Code'      => $values[$idx],
                'Display'   => $display[$idx],
                'Notes'     => '',
                'UrlPublic' => $urlPublic[$idx],
                'UrlApi'    => $urlApi[$idx],
//                'isActive'  => $isActive[$idx] ? 1 : 0,
                'Order'     => $isVisible[$idx] ? 1 : 0,
            ]);
        }
    }
}
