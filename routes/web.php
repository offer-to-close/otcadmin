<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});
Route::get('/admin/dashboard', 'DashboardController@mainDisplay')->name('dashboard.main');

Route::get('/oh/dashboard', 'DashboardController@switchToOh')->name('oh.main');
Route::get('/offer/dashboard', 'DashboardController@switchToOffer')->name('offer.main');


// ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- -----
// ... Offer To Close Actions
// ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- -----
Route::group(['prefix' => 'otc'],
    function ()
    {
        Route::get('/dashboard', 'DashboardController@switchToOtc')->name('otc.main');

        Route::match(['get', 'post'],'/review-member-request', 'OTC\MenuController@reviewMemberRequests')->name('otc.reviewMemberRequest');

        Route::match(['get', 'post'],'/process/menu/{path?}', 'StaffToolsController@processToolMenu')->name('otc.menuProcess');
        Route::match(['get', 'post'],'/approve-request/{memReqID?}', 'OTC\MenuController@approveRequest')->name('otc.approveRequest');
        Route::match(['get', 'post'],'/reject-requestReason/{memReqID?}', 'OTC\MenuController@rejectRequestReason')->name('otc.rejectRequestReason');
        Route::match(['get', 'post'],'/reject-request/{memReqID?}', 'OTC\MenuController@rejectRequest')->name('otc.rejectRequest');


        Route::post('/invite/send', 'OTC\MenuController@sendInvitationFromRequest')->name('otc.invite.send');
        Route::post('/request/reject', 'OTC\MenuController@rejectRequest')->name('otc.request.reject');
        Route::get('/mark/request/spam/{ID}', 'OTC\MenuController@markRequestSpam')->name('otc.markRequestSpam');
    });


// ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- -----
// ... Payment Actions
// ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- -----
Route::group(['prefix' => 'pmt'],
    function ()
    {
        Route::any('/test/stripe', 'Payments\PaymentController@testStripeFactory')->name('pmt.test.stripe');
    });


// ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- -----
// ... otcOpenHouse Actions
// ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- -----
//Route::group(['prefix' => 'oh'],
//    function ()
//    {
//        Route::post('/', 'OH\Controller@reviewMemberRequests')->name('otc.reviewMemberRequest');
//
//    });


// ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- -----
// ... otcOffers Actions
// ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- -----
//Route::group(['prefix' => 'offers'],
//    function ()
//    {
//        Route::post('/review-member-request', 'OTC\Controller@reviewMemberRequests')->name('otc.reviewMemberRequest');
//
//    });

// ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- -----
// ... Routes used for testing
// ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- -----
include_once(__DIR__ . DIRECTORY_SEPARATOR . 'test.php');
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Auth::routes();

Route::get('/entry', 'Auth\LoginController@showLoginForm')->name('entry');
Route::post('/entry/x', 'Auth\LoginController@login')->name('login');
Route::match(['get', 'post'],'/login', 'DashboardController@offerToClose');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');