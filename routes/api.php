<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// ... get OTC Common constants
Route::get('constants/{apiUserID}/{constant?}', [
    'middleware' => 'App\Http\Middleware\apiAccess',
    'uses'       => 'ApiController@contants',
]);


// ... Payment API
// ... http://otcAdmin.com/api/pmt/<accessID>/<rest>/<of>/<endpoint>
//
Route::group(['prefix' => 'pmt'],
    function ()
    {
        Route::any('{apiUserID}/test/stripe', [
            'middleware' => 'App\Http\Middleware\apiAccess',
            'uses'       => 'Payments\PaymentController@testStripeFactory',
        ]);

        Route::any('{apiUserID}/charge/{service}/{chargeType}/{payload}', [
            'middleware' => 'App\Http\Middleware\apiAccess',
            'uses'       => 'Payments\PaymentController@charge',
        ]);
    });


// ... WebHook Third Party Callback API
// ... http://otcAdmin.com/api/wh/<accessID>/ ...
//
Route::group(['prefix' => 'wh'],
    function ()
    {
        Route::any('{apiUserID}/stripe/{stripeFunction?}/{payload?}', [
            'middleware' => 'App\Http\Middleware\apiAccess',
            'uses'       => 'Payments\PaymentController@stripeCallback',
        ]);
    });

Route::get('stripe', 'Payments\PaymentController@stripe');
Route::post('stripe', 'Payments\PaymentController@stripePost')->name('stripe.post');
Route::post('stripe/checkout/success', 'Payments\PaymentController@checkoutSuccess')->name('stripe.checkout.success');
Route::post('stripe/checkout/cancel', 'Payments\PaymentController@checkoutSuccess')->name('stripe.checkout.cancel');
